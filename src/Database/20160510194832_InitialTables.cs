    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace NimbleHunterRH.Database
{
    [Migration(20160510194832)]
    public class Migration20160510194832 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_states", t =>
            {
                t.AddString("state_name").WithSize(30);
                t.AddString("sigla").WithSize(3).NotNullable();
            });

            schema.AddTable("t_cities", t =>
            {
                t.AddString("city_name").WithSize(30);
                t.AddBoolean("capital").NotNullable().Default(false);
                t.AddInt32("id_state").NotNullable().AutoForeignKey("t_states");
            });

            schema.AddTable("t_enterprises", t =>
            {
                t.AddDateTime("creation_date").NotNullable();
                t.AddString("name").WithSize(60).NotNullable();
                t.AddString("cnpj").WithSize(14).NotNullable();
                t.AddString("token").WithSize(30).NotNullable();
                t.AddString("cep").WithSize(8);
                t.AddString("neighbohood").WithSize(30);
                t.AddString("company_website").WithSize(100);
                t.AddBoolean("is_multinacional").Default(false).NotNullable();
                t.AddString("phone").WithSize(11).NotNullable();
                t.AddInt32("enum_line_business").NotNullable();
                t.AddInt32("id_city").AutoForeignKey("t_cities");
            });

            schema.AddTable("t_vacancies", t =>
            {

                t.AddString("ocupation").WithSize(30).NotNullable();
                t.AddBoolean("is_vacancy_deficient").Default(false).NotNullable();
                t.AddBoolean("is_visible_candidate").Default(true).NotNullable();
                t.AddString("description");
                t.AddInt32("enum_status").NotNullable();
                t.AddInt32("id_enterprise").NotNullable().AutoForeignKey("t_enterprises");
                t.AddInt32("id_city").AutoForeignKey("t_cities");
            });

            schema.AddTable("t_steps_vacancies", t =>
            {
                t.AddInt32("enum_step").NotNullable();
                t.AddDateTime("initial_date").NotNullable();
                t.AddDateTime("end_time").NotNullable();
                t.AddInt32("enum_status").NotNullable();
                t.AddInt32("id_vacancy").NotNullable().AutoForeignKey("t_vacancies");
            });

            schema.AddTable("t_users", t =>
            {
                t.AddDateTime("creation_date").NotNullable();
                t.AddString("cpf").WithSize(11).NotNullable();
                t.AddBinary("password").NotNullable().WithSize(48);
                t.AddInt32("enum_profile").NotNullable();
                t.AddString("full_name").WithSize(100).NotNullable();
                t.AddDateTime("birth_date");
                t.AddString("nationality").WithSize(30);
                t.AddInt32("enum_genre");
                t.AddInt32("enum_marital_status");
                t.AddBoolean("is_deficient").Default(false).NotNullable();
                t.AddString("address").WithSize(30);
                t.AddString("cep").WithSize(8);
                t.AddString("number").WithSize(10);
                t.AddString("complement").WithSize(20);
                t.AddString("neighborhood").WithSize(30);
                t.AddString("email").WithSize(20);
                t.AddString("home_phone").WithSize(11);
                t.AddString("cell_phone").WithSize(11);
                t.AddString("optional_phone").WithSize(11);
                t.AddInt32("id_city").AutoForeignKey("t_cities");
                t.AddString("url_photo").WithSize(200);
                t.AddString("token").WithSize(30).NotNullable();
                t.AddInt32("id_enterprise").NotNullable().AutoForeignKey("t_enterprises");
            });

            schema.AddTable("t_curriculums", t =>
            {
                t.AddInt32("enum_first_area_interest").NotNullable();
                t.AddInt32("enum_second_area_interest");
                t.AddInt32("enum_third_area_interest");
                t.AddInt32("enum_salary_requeriments");
                t.AddBoolean("worked_company").Default(false).NotNullable();
                t.AddString("sector_worked").WithSize(30);
                t.AddDateTime("admition_date_work");
                t.AddDateTime("termination_date_work");
                t.AddString("termination_reason_work").WithSize(30);
                t.AddBoolean("has_kinship_worked").Default(false).NotNullable();
                t.AddInt32("enum_degree_kinship");
                t.AddString("name_kinship").WithSize(30);
                t.AddString("sector_kinship").WithSize(30);
                t.AddString("phone_kinship").WithSize(11);
                t.AddDateTime("last_modification").NotNullable();
                t.AddDateTime("creation_date").NotNullable();
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
                t.AddInt32("id_entreprise").NotNullable().AutoForeignKey("t_enterprises");
                t.AddInt32("enum_status_curriculum").NotNullable();
            });

            schema.AddTable("t_extra_activities", t =>
            {
                t.AddInt32("enum_type_activity").NotNullable();
                t.AddDateTime("initial_date").NotNullable();
                t.AddDateTime("end_date");
                t.AddInt32("enum_country").NotNullable();
                t.AddString("description");
                t.AddString("name_city_country").WithSize(30);
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
                t.AddInt32("id_city").AutoForeignKey("t_cities");
            });

            schema.AddTable("t_certificates", t =>
            {
                t.AddString("certification").WithSize(20).NotNullable();
                t.AddInt32("number_organ_class").NotNullable();
                t.AddString("organ_issuer").WithSize(20).NotNullable();
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
            });

            schema.AddTable("t_academic_formations", t =>
            {
                t.AddInt32("enum_type_formation").NotNullable();
                t.AddInt32("enum_country").NotNullable();
                t.AddString("course").WithSize(30).NotNullable();
                t.AddString("institute").WithSize(30).NotNullable();
                t.AddInt32("enum_status_formation").NotNullable();
                t.AddDateTime("initial_date");
                t.AddString("name_city_country").WithSize(30);
                t.AddDateTime("end_date");
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
                t.AddInt32("id_city").AutoForeignKey("t_cities");
            });

            schema.AddTable("t_professional_courses", t =>
            {
                t.AddString("course").WithSize(30).NotNullable();
                t.AddInt32("enum_country").NotNullable();
                t.AddString("Institute").WithSize(30).NotNullable();
                t.AddInt32("workload");
                t.AddString("name_city_country").WithSize(30);
                t.AddDateTime("conclusion_date");
                t.AddInt32("id_city").AutoForeignKey("t_cities");
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
            });

            schema.AddTable("t_professional_experiences", t =>
            {
                t.AddString("company").WithSize(40).NotNullable();
                t.AddString("occupation").WithSize(30).NotNullable();
                t.AddInt32("enum_occupation_area").NotNullable();
                t.AddDateTime("admission_date").NotNullable();
                t.AddDateTime("termination_date").NotNullable();
                t.AddString("name_city_country").WithSize(30);
                t.AddString("immediate_superior").WithSize(30);
                t.AddString("company_phone").WithSize(11);
                t.AddInt32("enum_country").NotNullable();
                t.AddDouble("last_salary");
                t.AddString("termination_reason").WithSize(30);
                t.AddString("activities_performed");
                t.AddInt32("id_city").AutoForeignKey("t_cities");
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
            });

            schema.AddTable("t_knowledge_languages", t =>
            {
                t.AddInt32("enum_language").NotNullable();
                t.AddInt32("enum_conversation").NotNullable();
                t.AddInt32("enum_reading").NotNullable();
                t.AddInt32("enum_writing").NotNullable();
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
            });

            schema.AddTable("t_knowledge_informatics", t =>
            {
                t.AddString("software").WithSize(40).NotNullable();
                t.AddInt32("enum_level").NotNullable();
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
            });

            schema.AddTable("t_selective_processes", t =>
            {
                t.AddString("observation");
                t.AddBoolean("is_approved").Default(false).NotNullable();
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
                t.AddInt32("id_step_vacancy").NotNullable().AutoForeignKey("t_steps_vacancies");
            });

            schema.AddTable("t_additional_fields", t =>
            {
                t.AddString("name_field").WithSize(40).NotNullable();
                t.AddInt32("enum_type_field").NotNullable();
                t.AddInt32("id_vacancy").NotNullable().AutoForeignKey("t_vacancies");
            });

            schema.AddTable("t_answers_additional_fields", t =>
            {
                t.AddString("answer").NotNullable();
                t.AddInt32("id_additional_field").NotNullable().AutoForeignKey("t_additional_fields");
                t.AddInt32("id_curriculum").NotNullable().AutoForeignKey("t_curriculums");
            });
        }
        

        public override void Down(SchemaAction schema)
        {
            schema.RemoveTable("t_answers_additional_fields");
            schema.RemoveTable("t_additional_fields");
            schema.RemoveTable("t_professional_objectives");
            schema.RemoveTable("t_selective_processes");
            schema.RemoveTable("t_knowledge_informatics");
            schema.RemoveTable("t_knowledge_languages");
            schema.RemoveTable("t_professional_experiences");
            schema.RemoveTable("t_professional_courses");
            schema.RemoveTable("t_academic_formations");
            schema.RemoveTable("t_certificates");
            schema.RemoveTable("t_extra_activities");
            schema.RemoveTable("t_personal_datas");
            schema.RemoveTable("t_curriculums");
            schema.RemoveTable("t_users");
            schema.RemoveTable("t_steps_vacancies");
            schema.RemoveTable("t_vacancies");
            schema.RemoveTable("t_enterprises");

        }
    }

}