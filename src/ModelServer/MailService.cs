﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using FluentEmail;

namespace NimbleHunterRH
{
    public class MailService
    {
        public string Template { get; set; }
        public string From { get; set; }
        public string FromName { get; set; }
        public string To { get; set; }
        public string ToName { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public void Send()
        {
            Email
                .From(From, !string.IsNullOrEmpty(FromName) ? FromName : "")
                .BCC(To, !string.IsNullOrEmpty(ToName) ? ToName : "")
                .Subject(Subject)
                .UsingClient(new SmtpClient())
                .Body(Message)
                .Send();
        }

        public void Send<T>(T model)
        {
            Email
                .From(From, !string.IsNullOrEmpty(FromName) ? FromName : "")
                .BCC(To, !string.IsNullOrEmpty(ToName) ? ToName : "")
                .Subject(Subject)
                .UsingClient(new SmtpClient())
                .UsingTemplate(Template, model)
                .Send();
        }

        public void Send(string from, string fromName, string to, string toName, string subject, string message)
        {
            Email
                .From(from, fromName)
                .BCC(to, toName)
                .Subject(subject)
                .UsingClient(new SmtpClient())
                .Body(message)
                .Send();
        }
    }
}
