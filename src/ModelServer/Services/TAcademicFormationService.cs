using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TAcademicFormationService : EntityService<TAcademicFormation>, ITAcademicFormationService
    {
        public TAcademicFormation create(int idCurriculum, TypeFormation typeFormation, Country country, string course, string institute, StatusFormation statusFormation, DateTime initialDate, DateTime endDate)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var academicFormation = new TAcademicFormation()
            {
                Curriculum = curriculum,
                EnumTypeFormation = typeFormation,
                EnumCountry = country,
                Course = course,
                Institute = institute,
                EnumStatusFormation = statusFormation,
                InitialDate = initialDate,
                EndDate = endDate
            }.Save();
            return academicFormation;
        }

        public TAcademicFormation Edit(int idAcademicFormation, TypeFormation typeFormation, Country country, string course, string institute, StatusFormation statusFormation, DateTime initialDate, DateTime endDate)
        {
            var original = TAcademicFormation.Find(x => x.Id == idAcademicFormation);
            original.EnumTypeFormation = typeFormation;
            original.Course = course;
            original.EnumCountry = country;
            original.InitialDate = initialDate;
            original.EndDate = endDate;
            original.Institute = institute;
            original.EnumStatusFormation = statusFormation;
            original.Update();
            return original;
        }

        public TAcademicFormation FindById(int id)
        {
            return TAcademicFormation.Find(x => x.Id == id);
        }

        public TAcademicFormation Remove(int idAcademicFormation)
        {
            var academicFormation = TAcademicFormation.Load(idAcademicFormation);
            academicFormation.Delete();
            return academicFormation;
        }

        public List<TCurriculum> SearchCurriculums(StatusFormation? statusFormation, TypeFormation? typeFormation, string course, string institute, Country? country, TCity city, string nameCityCountry)
        {
            var criteria = Session.CreateCriteria<TAcademicFormation>();

            if (statusFormation.HasValue && statusFormation != 0)
                criteria.Add(Restrictions.Eq("EnumStatusFormation", statusFormation));

            if (typeFormation.HasValue && typeFormation != 0)
                criteria.Add(Restrictions.Eq("EnumTypeFormation", typeFormation));

            if (!string.IsNullOrWhiteSpace(course))
                criteria.Add(Restrictions.InsensitiveLike("Course", course, MatchMode.Anywhere));

            if (!string.IsNullOrWhiteSpace(institute))
                criteria.Add(Restrictions.InsensitiveLike("Institute", institute, MatchMode.Anywhere));

            if (country.HasValue && country != 0)
                criteria.Add(Restrictions.Eq("EnumCountry", country));

            if (city != null)
                criteria.Add(Restrictions.Eq("City", city));
            else
            {
                if (!string.IsNullOrWhiteSpace(nameCityCountry))
                    criteria.Add(Restrictions.InsensitiveLike("NameCityCountry", nameCityCountry, MatchMode.Anywhere));
            }

            return criteria.List<TAcademicFormation>().Select(x => x.Curriculum).ToList();
        }
    }
}