using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;
using NVelocity.Runtime.Parser;

namespace NimbleHunterRH.Services
{
    public partial class TUserService : EntityService<TUser>, ITUserService
    {
        public TUser create(string password, string cpf, int idEnterprise, Profile profile)
        {
            var enterprise = TEnterprise.Load(idEnterprise);
            var user = new TUser()
            {
                CreationDate = DateTime.Now,
                Password = HashPassword(password),
                Cpf = cpf,
                Token = Guid.NewGuid().ToString(),
                Enterprise = enterprise,
                EnumProfile = profile
            }.Save();
            return user;
        }

        public TUser create(string password, string cpf)
        {
            throw new NotImplementedException();
        }

        public TUser EditCpf(string cpf, string newCpf)
        {
            var original = TUser.Find(x => x.Cpf.Equals(cpf));
            original.Cpf = newCpf;
            original.Update();
            return original;
        }

        public TUser FindById(int id)
        {
            return TUser.Find(x => x.Id == id);
        }

        public TUser FindByCpf(string cpf)
        {
            return TUser.Find(x => x.Cpf == cpf);
        }

        public byte[] HashPassword(string password)
        {
            return SHA384.Create().ComputeHash(Encoding.UTF8.GetBytes(password));
        }

        public TUser Authenticate(string cpf, string password)
        {
            var user = FindByCpf(cpf);
            if (user == null || string.IsNullOrWhiteSpace(password))
                return null;

            bool passwordCorrect = false;
            passwordCorrect = this.HashPassword(password).SequenceEqual(user.Password);

            if (!passwordCorrect)
                return null;
            else
                return user;
        }

        public TUser Remove(int idUser)
        {
            var user = TUser.Load(idUser);
            if (user.TCurriculums.Any())
            {
                foreach (var curriculum in user.TCurriculums)
                {
                    curriculum.Delete();
                }
            }
            user.Delete();
            return user;
        }

        public List<TUser> ListAll()
        {
            List<TUser> users = new List<TUser>();
            users = TUser.List(x => x.Id != SecurityContext.Do.User.Id).OrderBy(y => y.Id).ToList();
            return users;
        }

        public List<TCurriculum> SearchCurriculums(string name, Genre? genre, MaritalStatus? maritalStatus, TCity city)
        {
            var criteria = Session.CreateCriteria<TUser>();

            if (!string.IsNullOrWhiteSpace(name))
                criteria.Add(Restrictions.Like("FullName", "%"+name+"%"));

               if (genre.HasValue && genre != 0)
                criteria.Add(Restrictions.Eq("EnumGenre", genre));

            if (maritalStatus.HasValue && maritalStatus != 0)
                criteria.Add(Restrictions.Eq("EnumMaritalStatus", maritalStatus));

            if (city != null)
                criteria.Add(Restrictions.Eq("City", city));

            return criteria.List<TUser>().Where(x=>x.EnumProfile == Profile.Candidato).Select(x => x.TCurriculums.FirstOrDefault()).ToList();
        }
    }
}