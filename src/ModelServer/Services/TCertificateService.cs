using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TCertificateService : EntityService<TCertificate>, ITCertificateService
    {
        public TCertificate create(int idCurriculum, string certification, int numberOrganClass, string organIssuer)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var certificate = new TCertificate()
            {
                Certification = certification,
                NumberOrganClass = numberOrganClass,
                OrganIssuer = organIssuer,
                Curriculum = curriculum
            }.Save();
            return certificate;
        }

        public TCertificate Edit(int idCertificate, string certification, int numberOrganClass, string organIssuer)
        {
            var original = TCertificate.Find(x => x.Id == idCertificate);
            original.Certification = certification;
            original.NumberOrganClass = numberOrganClass;
            original.OrganIssuer = organIssuer;
            original.Update();
            return original;
        }

        public TCertificate FindById(int id)
        {
            return TCertificate.Find(x => x.Id == id);
        }

        public TCertificate Remove(int idCertificate)
        {
            var certificate = TCertificate.Load(idCertificate);
            certificate.Delete();
            return certificate;
        }

        public List<TCurriculum> SearchCurriculums(string certificate, string organ, int number)
        {
            var criteria = Session.CreateCriteria<TCertificate>();
            if (!string.IsNullOrWhiteSpace(certificate))
                criteria.Add(Restrictions.InsensitiveLike("Certification", certificate, MatchMode.Anywhere));

            if (!string.IsNullOrWhiteSpace(organ))
                criteria.Add(Restrictions.InsensitiveLike("OrganIssuer", organ, MatchMode.Anywhere));
                criteria.Add(Restrictions.InsensitiveLike("OrganIssuer", organ, MatchMode.Anywhere));

            if (number != 0)
                criteria.Add(Restrictions.Eq("NumberOrganClass", number));

            return criteria.List<TCertificate>().Select(x => x.Curriculum).ToList();
        } 
    }
}