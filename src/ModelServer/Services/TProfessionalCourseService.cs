using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TProfessionalCourseService : EntityService<TProfessionalCourse>, ITProfessionalCourseService
    {
        public TProfessionalCourse create(int idCurriculum, DateTime conclusionDate, TCity city, Country country, string course, string institute, int workload)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var professionalCourse = new TProfessionalCourse()
            {
                ConclusionDate = conclusionDate,
                City = city,
                Curriculum = curriculum,
                EnumCountry = country,
                Course = course,
                Institute = institute,
                Workload = workload
            }.Save();
            return professionalCourse;
        }

        public TProfessionalCourse Edit(int idProfessionalCourse, DateTime conclusionDate, TCity city, Country country, string course, string institute, int workload)
        {
            var original = TProfessionalCourse.Find(x => x.Id == idProfessionalCourse);
            original.ConclusionDate = conclusionDate;
            original.City = city;
            original.Course = course;
            original.Institute = institute;
            original.Workload = workload;
            original.EnumCountry = country;
            original.Update();
            return original;
        }

        public TProfessionalCourse FindById(int id)
        {
            return TProfessionalCourse.Find(x => x.Id == id);
        }

        public TProfessionalCourse Remove(int idProfessionalCourse)
        {
            var professionalCourse = TProfessionalCourse.Load(idProfessionalCourse);
            professionalCourse.Delete();
            return professionalCourse;
        }

        public List<TCurriculum> SearchCurriculums(string course, string institute, Country? country, TCity city, string nameCityCountry)
        {
            var criteria = Session.CreateCriteria<TProfessionalCourse>();

            if (!string.IsNullOrWhiteSpace(course))
                criteria.Add(Restrictions.InsensitiveLike("Course", course, MatchMode.Anywhere));

            if (!string.IsNullOrWhiteSpace(institute))
                criteria.Add(Restrictions.InsensitiveLike("Institute", institute, MatchMode.Anywhere));

            if (country.HasValue && country != 0)
                criteria.Add(Restrictions.Eq("EnumCountry", country));

            if (city != null)
                criteria.Add(Restrictions.Eq("City", city));
            else
            {
                if (!string.IsNullOrWhiteSpace(nameCityCountry))
                    criteria.Add(Restrictions.Eq("NameCityCountry", nameCityCountry));
            }

            return criteria.List<TProfessionalCourse>().Select(x => x.Curriculum).ToList();
        }
    }
}