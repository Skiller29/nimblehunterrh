using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TKnowledgeLanguageService : EntityService<TKnowledgeLanguage>, ITKnowledgeLanguageService
    {
        public TKnowledgeLanguage create(int idCurriculum, Language language, LevelLanguage reading, LevelLanguage writing, LevelLanguage conversation)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var knowledgeLanguage = new TKnowledgeLanguage()
            {
                Curriculum = curriculum,
                EnumConversation = conversation,
                EnumLanguage = language,
                EnumReading = reading,
                EnumWriting = writing
            }.Save();
            return knowledgeLanguage;
        }

        public TKnowledgeLanguage Edit(int idKnowledgeLanguage, Language language, LevelLanguage reading, LevelLanguage writing, LevelLanguage conversation)
        {
            var original = TKnowledgeLanguage.Find(x => x.Id == idKnowledgeLanguage);
            original.EnumLanguage = language;
            original.EnumReading = reading;
            original.EnumWriting = writing;
            original.EnumConversation = conversation;
            original.Update();
            return original;
        }

        public TKnowledgeLanguage FindById(int id)
        {
            return TKnowledgeLanguage.Find(x => x.Id == id);
        }

        public TKnowledgeLanguage Remove(int idKnowledgeLanguage)
        {
            var knowledgeLanguage = TKnowledgeLanguage.Load(idKnowledgeLanguage);
            knowledgeLanguage.Delete();
            return knowledgeLanguage;
        }

        public List<TCurriculum> SearchCurriculums(Language? language, LevelLanguage? conversation, LevelLanguage? writing, LevelLanguage? reading)
        {
            var criteria = Session.CreateCriteria<TKnowledgeLanguage>();

            if(language.HasValue && language != 0)
                criteria.Add(Restrictions.Eq("EnumLanguage", language));
            if(conversation.HasValue)
                criteria.Add(Restrictions.Eq("EnumConversation", conversation));
            if(reading.HasValue)
                criteria.Add(Restrictions.Eq("EnumReading", reading));
            if (writing.HasValue)
                criteria.Add(Restrictions.Eq("EnumWriting", writing));

            return criteria.List<TKnowledgeLanguage>().Select(x => x.Curriculum).ToList();

        } 
    }
}