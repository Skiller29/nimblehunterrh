using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TVacancyService : EntityService<TVacancy>, ITVacancyService
    {
        public TVacancy create(TCity city, string occupation, int numberJobs, bool isVacancyDeficient, bool isVisibleCandidate, string description, TEnterprise enterprise)
        {
            var vacancy = new TVacancy()
            {
                City = city,
                Ocupation = occupation,
                IsVacancyDeficient = isVacancyDeficient,
                IsVisibleCandidate = isVisibleCandidate,
                Description = description,
                Enterprise = enterprise,
            }.Save();
            return vacancy;
        }

        public TVacancy Edit(int idVacancy, TCity city, string occupation, int numberJobs, bool isVacancyDeficient, bool isVisibleCandidate, string description)
        {
            var original = TVacancy.Find(x => x.Id == idVacancy);
            original.City = city;
            original.Ocupation = occupation;
            original.IsVacancyDeficient = isVacancyDeficient;
            original.IsVisibleCandidate = isVisibleCandidate;
            original.Description = description;
            original.Update();
            return original;
        }

        public TVacancy FindById(int id)
        {
            return TVacancy.Find(x => x.Id == id);
        }

        public TVacancy Remove(int idVacancy)
        {
            throw new NotImplementedException();
        }
    }
}