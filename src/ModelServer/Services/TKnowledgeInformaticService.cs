using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Services
{
    public partial class TKnowledgeInformaticService : EntityService<TKnowledgeInformatic>, ITKnowledgeInformaticService
    {
        public TKnowledgeInformatic create(int idCurriculum, string software, Level level)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var knowledgeInformatic = new TKnowledgeInformatic()
            {
                Curriculum = curriculum,
                Software = software,
                EnumLevel = level
            }.Save();
            return knowledgeInformatic;
        }

        public TKnowledgeInformatic Edit(int idKnowledgeInformaticy, string software, Level level)
        {
            var original = TKnowledgeInformatic.Find(x => x.Id == idKnowledgeInformaticy);
            original.Software = software;
            original.EnumLevel = level;
            original.Update();
            return original;
        }

        public TKnowledgeInformatic FindById(int id)
        {
            return TKnowledgeInformatic.Find(x => x.Id == id);
        }

        public TKnowledgeInformatic Remove(int idKnowledgeInformaticy)
        {
            var knowledgeInformatic = TKnowledgeInformatic.Load(idKnowledgeInformaticy);
            knowledgeInformatic.Delete();
            return knowledgeInformatic;
        }

        public List<TCurriculum> SearchCurriculums(string software, Level? level)
        {
            var criteria = Session.CreateCriteria<TKnowledgeInformatic>();
            if (!string.IsNullOrWhiteSpace(software))
                criteria.Add(Restrictions.InsensitiveLike("Software", software, MatchMode.Anywhere));
            if (level.HasValue)
                criteria.Add(Restrictions.Eq("EnumLevel", level));

            return criteria.List<TKnowledgeInformatic>().Select(x => x.Curriculum).ToList();
        }
    }
}