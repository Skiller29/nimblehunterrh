using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TCurriculumService : EntityService<TCurriculum>, ITCurriculumService
    {
        public TCurriculum create(int idUser, int idEnterprise, DateTime admitionDateWork, DegreeKinship degreeKinship, StatusCurriculum statusCurriculum, bool hasKinshipWorked, string nameKinship, string phoneKinship, string sectorKinship, string sectorWorked, string terminationReasonWork, DateTime terminationDateWork, string photoUrl, bool workedCompany)
        {
            var user = TUser.Load(idUser);
            var enterprise = TEnterprise.Load(idEnterprise);
            var curriculum = new TCurriculum()
            {
                User = user,
                AdmitionDateWork = admitionDateWork,
                CreationDate = DateTime.Now,
                Entreprise = enterprise,
                EnumDegreeKinship = degreeKinship,
                EnumStatusCurriculum = statusCurriculum,
                HasKinshipWorked = hasKinshipWorked,
                LastModification = DateTime.Now,
                NameKinship = nameKinship,
                PhoneKinship =  phoneKinship,
                SectorKinship = sectorKinship,
                SectorWorked = sectorWorked,
                TerminationDateWork = terminationDateWork,
                TerminationReasonWork = terminationReasonWork,
                WorkedCompany = workedCompany
            }.Save();
            return curriculum;
        }

        public TCurriculum Edit(int idCurriculum, DateTime admitionDateWork, DegreeKinship degreeKinship, StatusCurriculum statusCurriculum, bool hasKinshipWorked, string nameKinship, string phoneKinship, string sectorKinship, string sectorWorked, string terminationReasonWork, DateTime terminationDateWork, string photoUrl, bool workedCompany)
        {
            var original = TCurriculum.Find(x => x.Id == idCurriculum);
            original.AdmitionDateWork = admitionDateWork;
            original.EnumDegreeKinship = degreeKinship;
            original.EnumStatusCurriculum = statusCurriculum;
            original.HasKinshipWorked = hasKinshipWorked;
            original.LastModification = DateTime.Now;
            original.NameKinship = nameKinship;
            original.PhoneKinship = phoneKinship;
            original.SectorKinship = sectorKinship;
            original.SectorWorked = sectorWorked;
            original.TerminationDateWork = terminationDateWork;
            original.TerminationReasonWork = terminationReasonWork;
            original.WorkedCompany = workedCompany;
            original.Update();
            return original;
        }

        public TCurriculum FindById(int id)
        {
            return TCurriculum.Find(x => x.Id == id);
        }

        public TCurriculum Remove(int idCurriculum)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            curriculum.Delete();
            return curriculum;
        }

        public List<TCurriculum> SearchCurrilums(AreaInterest? firstAreaInterest, AreaInterest? secondAreaInterest, AreaInterest? thiAreaInterest, SalaryRequeriments? salary)
        {
            var criteria = Session.CreateCriteria<TCurriculum>();

            if (firstAreaInterest.HasValue && firstAreaInterest != 0)
                criteria.Add(Restrictions.Eq("EnumFirstAreaInterest", firstAreaInterest.Value));

            if (secondAreaInterest.HasValue && secondAreaInterest != 0)
                criteria.Add(Restrictions.Eq("EnumSecondAreaInterest", secondAreaInterest));

            if (thiAreaInterest.HasValue && thiAreaInterest != 0)
                criteria.Add(Restrictions.Eq("EnumThirdAreaInterest", thiAreaInterest));

            if (salary.HasValue && salary != 0)
                criteria.Add(Restrictions.Eq("EnumSalaryRequeriments", salary));

            return criteria.List<TCurriculum>().ToList();
        }
             
    }
}