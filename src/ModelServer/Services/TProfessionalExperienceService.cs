using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TProfessionalExperienceService : EntityService<TProfessionalExperience>, ITProfessionalExperienceService
    {
        public TProfessionalExperience create(int idCurriculum, string company, string occupation, OccupationArea occupationArea, string immediateSuperior, string companyPhone, Country country, double lastSalary, string terminationReason, string actvitiesPerformed, DateTime admissionDate, DateTime terminatioDate, TCity city)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var professionalExperience = new TProfessionalExperience()
            {
                Curriculum = curriculum,
                Company = company,
                EnumCountry = country,
                Occupation = occupation,
                EnumOccupationArea = occupationArea,
                ActivitiesPerformed = actvitiesPerformed,
                AdmissionDate = admissionDate,
                TerminationDate = terminatioDate,
                TerminationReason = terminationReason,
                CompanyPhone = companyPhone,
                ImmediateSuperior = immediateSuperior,
                LastSalary = lastSalary,
                City = city,
            }.Save();
            return professionalExperience;
        }

        public TProfessionalExperience Edit(int idProfessionalExperience, string company, string occupation, OccupationArea occupationArea, string immediateSuperior, string companyPhone, Country country, double lastSalary, string terminationReason, string actvitiesPerformed, DateTime admissionDate, DateTime terminatioDate, TCity city)
        {
            var original = TProfessionalExperience.Find(x => x.Id == idProfessionalExperience);
            original.Company = company;
            original.Occupation = occupation;
            original.EnumOccupationArea = occupationArea;
            original.AdmissionDate = admissionDate;
            original.TerminationDate = terminatioDate;
            original.TerminationReason = terminationReason;
            original.ImmediateSuperior = immediateSuperior;
            original.CompanyPhone = companyPhone;
            original.LastSalary = lastSalary;
            original.City = city;
            original.EnumCountry = country;
            original.ActivitiesPerformed = actvitiesPerformed;
            original.Update();
            return original;
        }

        public TProfessionalExperience FindById(int id)
        {
            return TProfessionalExperience.Find(x => x.Id == id);
        }

        public TProfessionalExperience Remove(int idProfessionalExperience)
        {
            var professionalExperience = TProfessionalExperience.Load(idProfessionalExperience);
            professionalExperience.Delete();
            return professionalExperience;
        }

        public List<TCurriculum> SearchCurriculums(OccupationArea? occupationArea, string occupation, string company, Country? country, TCity city, string nameCityCountry)
        {
            var criteria = Session.CreateCriteria<TProfessionalExperience>();

            if (occupationArea.HasValue)
                criteria.Add(Restrictions.Eq("EnumOccupationArea", occupationArea));

            if (!string.IsNullOrWhiteSpace(occupation))
                criteria.Add(Restrictions.InsensitiveLike("Occupation", occupation, MatchMode.Anywhere));

            if (!string.IsNullOrWhiteSpace(company))
                criteria.Add(Restrictions.InsensitiveLike("Company", company, MatchMode.Anywhere));

            if (country.HasValue && country != 0)
                criteria.Add(Restrictions.Eq("EnumCountry", country));

            if (city != null)
                criteria.Add(Restrictions.Eq("City", city));
            else
            {
                if (!string.IsNullOrWhiteSpace(nameCityCountry))
                    criteria.Add(Restrictions.Eq("NameCityCountry", nameCityCountry));
            }

            return criteria.List<TProfessionalExperience>().Select(x => x.Curriculum).ToList();
        }
    }
}