using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TExtraActivityService : EntityService<TExtraActivity>, ITExtraActivityService
    {
        public TExtraActivity create(int idCurriculum, TypeActivity typeActivity, DateTime initialDate, DateTime endDate, Country country, string description, TCity city)
        {
            var curriculum = TCurriculum.Load(idCurriculum);
            var extraActivity = new TExtraActivity()
            {
                Curriculum = curriculum,
                EnumTypeActivity = typeActivity,
                EnumCountry = country,
                City = city,
                InitialDate = initialDate,
                EndDate = endDate,
                Description = description
            }.Save();
            return extraActivity;
        }

        public TExtraActivity Edit(int idExtraActivity, TypeActivity typeActivity, DateTime initialDate, DateTime endDate, Country country, string description, TCity city)
        {
            var original = TExtraActivity.Find(x => x.Id == idExtraActivity);
            original.EnumTypeActivity = typeActivity;
            original.InitialDate = initialDate;
            original.EndDate = endDate;
            original.City = city;
            original.Description = description;
            original.EnumCountry = country;
            original.Update();
            return original;
        }

        public TExtraActivity FindById(int id)
        {
            return TExtraActivity.Find(x => x.Id == id);
        }

        public TExtraActivity Remove(int idExtraActivity)
        {
            var extraActivity = TExtraActivity.Load(idExtraActivity);
            extraActivity.Delete();
            return extraActivity;
        }

        public List<TCurriculum> SearchCurriculums(TypeActivity? typeActivity, Country? country, TCity city, string nameCityCountry)
        {
            var criteria = Session.CreateCriteria<TExtraActivity>();

            if (typeActivity.HasValue)
                criteria.Add(Restrictions.Eq("EnumTypeActivity", typeActivity));

            if (country.HasValue && country != 0)
                criteria.Add(Restrictions.Eq("EnumCountry", country));

            if (city != null)
                criteria.Add(Restrictions.Eq("City", city));
            else
            {
                if (!string.IsNullOrWhiteSpace(nameCityCountry))
                    criteria.Add(Restrictions.InsensitiveLike("NameCityCountry", nameCityCountry, MatchMode.Anywhere));
            }       

            return criteria.List<TExtraActivity>().Select(x => x.Curriculum).ToList();
        }
    }
}