using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Entities;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Services
{
    public partial class TEnterpriseService : EntityService<TEnterprise>, ITEnterpriseService
    {
        public TEnterprise create(TCity city, string cnpj, string name, string cep, string phone, LineBusiness lineBusiness, bool isMultinacional = false, string logoUrl = null, string neighbhorood = null, string companyWebsite = null)
        {
            var enterprise = new TEnterprise()
            {
                Token = Guid.NewGuid().ToString(),
                City = city,
                Cnpj = cnpj,
                Name = name,
                Cep = cep,
                IsMultinacional = isMultinacional,
                Phone = phone,
                EnumLineBusiness = lineBusiness,
                Neighbohood = neighbhorood,
                CompanyWebsite = companyWebsite
            }.Save();
            return enterprise;
        }

        public TEnterprise Edit(int idEnterprise, TCity city, string cnpj, string name, string cep, string phone, LineBusiness lineBusiness, bool isMultinacional = false, string logoUrl = null, string neighbhorood = null, string companyWebsite = null)
        {
            var original = TEnterprise.Find(x=>x.Id == idEnterprise);
            original.City = city;
            original.Cnpj = cnpj;
            original.Name = name;
            original.Cep = cep;
            original.Phone = phone;
            original.EnumLineBusiness = lineBusiness;
            original.IsMultinacional = isMultinacional;
            original.Neighbohood = neighbhorood;
            original.CompanyWebsite = companyWebsite;
            original.Update();
            return original;
        }

        public TEnterprise FindById(int id)
        {
            return TEnterprise.Find(x => x.Id == id);
        }

        public TEnterprise Remove(int idEnterprise)
        {
            var enterprise = TEnterprise.Load(idEnterprise);
            if (enterprise.TVacancies.Any())
            {
                foreach (var vagas in enterprise.TVacancies)
                {
                    vagas.Delete();
                }
            }
            enterprise.Delete();
            return enterprise;
        }

        public List<TEnterprise> ListAll()
        {
            var enterprise = TEnterprise.ListAll().ToList();
            return enterprise;
        }
    }
}