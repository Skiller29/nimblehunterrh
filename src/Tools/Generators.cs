using Simple.Generator;
using NimbleHunterRH.Tools.Templates;
using NimbleHunterRH.Tools.Macros;
using NimbleHunterRH.Tools.Templates.Scaffold;
using NimbleHunterRH.Tools.Templates.View;
using NimbleHunterRH.Tools.Database;

namespace NimbleHunterRH.Tools
{
    public static class Generators
    {
        public static CommandResolver RegisterCommands(this CommandResolver registry, bool production)
        {
            registry.Register<MigrateTool>("migrate")
                .WithOption("to", x => x.Version)
                .WithOption("script", x => x.FilePath)
                .WithOption("env", x => x.Environment);

            registry.Register<InsertDataCommand>("data").WithOption("testdata", x => x.ForceTestData);
            registry.Register<PrepareMacro>("prepare");
            registry.Register<SystemCheckMacro>("check");

            if (production)
            {
                registry.Register<TestPrepareMacro>("testprepare");
            }
            else
            {
                registry.Register<ResetDbCommand>("resetdb")
                    .WithOption("prepare", x => x.Prepare);

                registry.Register<NewMigrationTemplate>("new migration")
                    .WithArgument("name", x => x.Name)
                    .WithOption("donotopen", x => x.OpenIt);

                registry.Register<MagicMacro>("magic");

                registry.Register<ScaffoldGenerator>("scaffold")
                    .WithArgumentList("tables", x => x.TableNames);

                registry.Register<ScaffoldRemover>("d scaffold")
                   .WithArgumentList("class_names", x => x.ClassNames);

                registry.Register<ViewGenerator>("view")
                    .WithArgumentList("class_names", x => x.ClassNames);


                registry.Register<RefreshMacro>("refresh");
            }

            return registry;
        }

     
    }
}
