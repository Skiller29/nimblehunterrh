using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Generator;
using NimbleHunterRH.Tools.Templates.Scaffold;

namespace NimbleHunterRH.Tools.Macros
{
    public class MagicMacro : ICommand
    {
        #region ICommand Members

        public void Execute()
        {
            new PrepareMacro().Execute();
            new ScaffoldGenerator().Execute();
        }

        #endregion
    }
}
