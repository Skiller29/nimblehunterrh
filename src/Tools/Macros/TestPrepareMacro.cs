using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Generator;
using log4net;
using Simple;
using System.Reflection;
using Simple.Generator.Console;
using NimbleHunterRH.Tools.Database;

namespace NimbleHunterRH.Tools.Macros
{
    public class TestPrepareMacro : IUnsafeCommand
    {
        ILog logger = Simply.Do.Log(MethodInfo.GetCurrentMethod());
        
        public void Execute()
        {
            using (Context.Test)
            {
                logger.Info("Migrating...");
                new MigrateTool() { Version = 1 }.Execute();
                new MigrateTool() { Version = null }.Execute();

                logger.Info("Executing...");
                new InsertDataCommand { ForceTestData = true }.Execute();
            }
        }
    }
}
