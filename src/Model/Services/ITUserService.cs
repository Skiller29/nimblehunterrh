using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITUserService : IEntityService<TUser>, IService
    {
        TUser create(String password, String cpf, Int32 idEnterprise, Profile profile);
        TUser create(String password, String cpf);
        TUser EditCpf(String cpf, String newCpf);
        TUser FindById(Int32 id);
        TUser FindByCpf(String cpf);
        Byte[] HashPassword(String password);
        TUser Authenticate(String cpf, String password);
        TUser Remove(Int32 idUser);
        List<TUser> ListAll();
        List<TCurriculum> SearchCurriculums(String name, Nullable<Genre> genre, Nullable<MaritalStatus> maritalStatus, TCity city);
    }
}