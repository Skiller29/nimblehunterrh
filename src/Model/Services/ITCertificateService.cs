using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITCertificateService : IEntityService<TCertificate>, IService
    {
        TCertificate create(Int32 idCurriculum, String certification, Int32 numberOrganClass, String organIssuer);
        TCertificate Edit(Int32 idCertificate, String certification, Int32 numberOrganClass, String organIssuer);
        TCertificate FindById(Int32 id);
        TCertificate Remove(Int32 idCertificate);
        List<TCurriculum> SearchCurriculums(String certificate, String organ, Int32 number);
    }
}