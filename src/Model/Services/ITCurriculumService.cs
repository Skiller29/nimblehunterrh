using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITCurriculumService : IEntityService<TCurriculum>, IService
    {
        TCurriculum create(Int32 idUser, Int32 idEnterprise, DateTime admitionDateWork, DegreeKinship degreeKinship, StatusCurriculum statusCurriculum, Boolean hasKinshipWorked, String nameKinship, String phoneKinship, String sectorKinship, String sectorWorked, String terminationReasonWork, DateTime terminationDateWork, String photoUrl, Boolean workedCompany);
        TCurriculum Edit(Int32 idCurriculum, DateTime admitionDateWork, DegreeKinship degreeKinship, StatusCurriculum statusCurriculum, Boolean hasKinshipWorked, String nameKinship, String phoneKinship, String sectorKinship, String sectorWorked, String terminationReasonWork, DateTime terminationDateWork, String photoUrl, Boolean workedCompany);
        TCurriculum FindById(Int32 id);
        TCurriculum Remove(Int32 idCurriculum);
        List<TCurriculum> SearchCurrilums(Nullable<AreaInterest> firstAreaInterest, Nullable<AreaInterest> secondAreaInterest, Nullable<AreaInterest> thiAreaInterest, Nullable<SalaryRequeriments> salary);
    }
}