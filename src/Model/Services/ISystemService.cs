using NimbleHunterRH.Services;
using Simple.Services;
using System.Collections.Generic;
using Simple.Patterns;

namespace NimbleHunterRH.Services
{
    public partial interface ISystemService : IService
    {
        IList<TaskRunner.Result> Check();
    }
}