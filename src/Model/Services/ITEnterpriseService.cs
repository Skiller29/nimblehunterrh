using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITEnterpriseService : IEntityService<TEnterprise>, IService
    {
        TEnterprise create(TCity city, String cnpj, String name, String cep, String phone, LineBusiness lineBusiness, Boolean isMultinacional, String logoUrl, String neighbhorood, String companyWebsite);
        TEnterprise Edit(Int32 idEnterprise, TCity city, String cnpj, String name, String cep, String phone, LineBusiness lineBusiness, Boolean isMultinacional, String logoUrl, String neighbhorood, String companyWebsite);
        TEnterprise FindById(Int32 id);
        TEnterprise Remove(Int32 idEnterprise);
        List<TEnterprise> ListAll();
    }
}