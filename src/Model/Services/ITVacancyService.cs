using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;

namespace NimbleHunterRH.Services
{
    public partial interface ITVacancyService : IEntityService<TVacancy>, IService
    {
        TVacancy create(TCity city, String occupation, Int32 numberJobs, Boolean isVacancyDeficient, Boolean isVisibleCandidate, String description, TEnterprise enterprise);
        TVacancy Edit(Int32 idVacancy, TCity city, String occupation, Int32 numberJobs, Boolean isVacancyDeficient, Boolean isVisibleCandidate, String description);
        TVacancy FindById(Int32 id);
        TVacancy Remove(Int32 idVacancy);
    }
}