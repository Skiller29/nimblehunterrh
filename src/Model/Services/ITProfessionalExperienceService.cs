using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITProfessionalExperienceService : IEntityService<TProfessionalExperience>, IService
    {
        TProfessionalExperience create(Int32 idCurriculum, String company, String occupation, OccupationArea occupationArea, String immediateSuperior, String companyPhone, Country country, Double lastSalary, String terminationReason, String actvitiesPerformed, DateTime admissionDate, DateTime terminatioDate, TCity city);
        TProfessionalExperience Edit(Int32 idProfessionalExperience, String company, String occupation, OccupationArea occupationArea, String immediateSuperior, String companyPhone, Country country, Double lastSalary, String terminationReason, String actvitiesPerformed, DateTime admissionDate, DateTime terminatioDate, TCity city);
        TProfessionalExperience FindById(Int32 id);
        TProfessionalExperience Remove(Int32 idProfessionalExperience);
        List<TCurriculum> SearchCurriculums(Nullable<OccupationArea> occupationArea, String occupation, String company, Nullable<Country> country, TCity city, String nameCityCountry);
    }
}