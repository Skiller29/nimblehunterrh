using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITKnowledgeLanguageService : IEntityService<TKnowledgeLanguage>, IService
    {
        TKnowledgeLanguage create(Int32 idCurriculum, Language language, LevelLanguage reading, LevelLanguage writing, LevelLanguage conversation);
        TKnowledgeLanguage Edit(Int32 idKnowledgeLanguage, Language language, LevelLanguage reading, LevelLanguage writing, LevelLanguage conversation);
        TKnowledgeLanguage FindById(Int32 id);
        TKnowledgeLanguage Remove(Int32 idKnowledgeLanguage);
        List<TCurriculum> SearchCurriculums(Nullable<Language> language, Nullable<LevelLanguage> conversation, Nullable<LevelLanguage> writing, Nullable<LevelLanguage> reading);
    }
}