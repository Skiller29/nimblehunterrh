using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITExtraActivityService : IEntityService<TExtraActivity>, IService
    {
        TExtraActivity create(Int32 idCurriculum, TypeActivity typeActivity, DateTime initialDate, DateTime endDate, Country country, String description, TCity city);
        TExtraActivity Edit(Int32 idExtraActivity, TypeActivity typeActivity, DateTime initialDate, DateTime endDate, Country country, String description, TCity city);
        TExtraActivity FindById(Int32 id);
        TExtraActivity Remove(Int32 idExtraActivity);
        List<TCurriculum> SearchCurriculums(Nullable<TypeActivity> typeActivity, Nullable<Country> country, TCity city, String nameCityCountry);
    }
}