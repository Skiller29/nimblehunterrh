using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITKnowledgeInformaticService : IEntityService<TKnowledgeInformatic>, IService
    {
        TKnowledgeInformatic create(Int32 idCurriculum, String software, Level level);
        TKnowledgeInformatic Edit(Int32 idKnowledgeInformaticy, String software, Level level);
        TKnowledgeInformatic FindById(Int32 id);
        TKnowledgeInformatic Remove(Int32 idKnowledgeInformaticy);
        List<TCurriculum> SearchCurriculums(String software, Nullable<Level> level);
    }
}