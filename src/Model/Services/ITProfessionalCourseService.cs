using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITProfessionalCourseService : IEntityService<TProfessionalCourse>, IService
    {
        TProfessionalCourse create(Int32 idCurriculum, DateTime conclusionDate, TCity city, Country country, String course, String institute, Int32 workload);
        TProfessionalCourse Edit(Int32 idProfessionalCourse, DateTime conclusionDate, TCity city, Country country, String course, String institute, Int32 workload);
        TProfessionalCourse FindById(Int32 id);
        TProfessionalCourse Remove(Int32 idProfessionalCourse);
        List<TCurriculum> SearchCurriculums(String course, String institute, Nullable<Country> country, TCity city, String nameCityCountry);
    }
}