using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Services
{
    public partial interface ITAcademicFormationService : IEntityService<TAcademicFormation>, IService
    {
        TAcademicFormation create(Int32 idCurriculum, TypeFormation typeFormation, Country country, String course, String institute, StatusFormation statusFormation, DateTime initialDate, DateTime endDate);
        TAcademicFormation Edit(Int32 idAcademicFormation, TypeFormation typeFormation, Country country, String course, String institute, StatusFormation statusFormation, DateTime initialDate, DateTime endDate);
        TAcademicFormation FindById(Int32 id);
        TAcademicFormation Remove(Int32 idAcademicFormation);
        List<TCurriculum> SearchCurriculums(Nullable<StatusFormation> statusFormation, Nullable<TypeFormation> typeFormation, String course, String institute, Nullable<Country> country, TCity city, String nameCityCountry);
    }
}