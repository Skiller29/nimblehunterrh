using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Services
{
    public partial interface ITAnswersAdditionalFieldService : IEntityService<TAnswersAdditionalField>, IService
    {
    }
}