﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum SalaryRequeriments
    {
        [Description("Até R$1000")]
        AteMil = 1,
        [Description("R$1.000 até R$2.000")]
        AteDoisMil,
        [Description("R$2.000 até R$3.000")]
        AteTresMil,
        [Description("R$3.000 até R$4.000")]
        AteQuatroMil,
        [Description("R$4.000 até R$5.000")]
        AteCincoMil,
        [Description("R$5.000 até R$6.000")]
        AteSeisMil,
        [Description("R$6.000 até R$7.000")]
        AteSeteMil,
        [Description("R$7.000 até R$8.000")]
        AteOitoMil,
        [Description("Acima de R$8.000")]
        AcimaNoveMil
    }
}
