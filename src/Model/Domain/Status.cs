﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum Status
    {
        [Description("Concluído")]
        Concluido = 0,
        [Description("Em andamento")]
        EmAndamento,
        [Description("Pendente")]
        Pendente
    }
}
