﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum Country
    {
        [Description("Afeganistão")]
        Afeganistao = 1,
        [Description("África do Sul")]
        AfricadoSul,
        [Description("Albânia")]
        Albania,
        [Description("Alemanha")]
        Alemanha,
        [Description("Andorra")]
        Andorra,
        [Description("Angola")]
        Angola,
        [Description("Antigua e Barbuda")]
        AntiguaBarbuda,
        [Description("Arábia Saudita")]
        ArabiaSaudita,
        [Description("Argélia")]
        Argelia,
        [Description("Argentina")]
        Argentina,
        [Description("Armênia")]
        Armenia,
        [Description("Austrália")]
        Australia,
        [Description("Áustria")]
        Austria,
        [Description("Azerbaijão")]
        Azerbaijao,
        [Description("Bahamas")]
        Bahamas,
        [Description("Bangladesh")]
        Bangladesh,
        [Description("Barbados")]
        Barbados,
        [Description("Bahrein")]
        Bahrein,
        [Description("Bélgica")]
        Belgica,
        [Description("Belize")]
        Belize,
        [Description("Benin")]
        Benin,
        [Description("Bielorrússia (Belarus)")]
        Bielorrussia,
        [Description("Bulgária")]
        Bolivia,
        [Description("Bósnia Herzegóvina")]
        BosniaHerzegovina,
        [Description("Botsuana")]
        Botsuana,
        [Description("Brasil")]
        Brasil,
        [Description("Brunei")]
        Brunei,
        [Description("Bulgária")]
        Bulgaria,
        [Description("Burkina-Fasso")]
        BurkinaFasso,
        [Description("Burundi")]
        Burundi,
        [Description("Butão")]
        Butão,
        [Description("Cabo Verde")]
        CaboVerde,
        [Description("Camarões")]
        Camaroes,
        [Description("Camboja")]
        Camboja,
        [Description("Canadá")]
        Canada,
        [Description("Catar")]
        Catar,
        [Description("Cazaquistão")]
        Cazaquistao,
        [Description("Chade")]
        Chade,
        [Description("Chile")]
        Chile,
        [Description("China")]
        China,
        [Description("Chipre")]
        Chipre,
        [Description("Cingapura")]
        Cingapura,
        [Description("Colômbia")]
        Colombia,
        [Description("Congo")]
        Congo,
        [Description("Comores")]
        Comores,
        [Description("Coréia do Norte")]
        CoreiadoNorte,
        [Description("Coréia do Sul")]
        CoreiadoSul,
        [Description("Costa do Marfim")]
        CostadoMarfim,
        [Description("Costa Rica")]
        CostaRica,
        [Description("Croácia")]
        Croacia,
        [Description("Cuba")]
        Cuba,
        [Description("Dinamarca")]
        Dinamarca,
        [Description("Djibuti")]
        Djibuti,
        [Description("Dominica")]
        Dominica,
        [Description("Egito")]
        Egito,
        [Description("El Salvador")]
        ElSalvador,
        [Description("Emirados Árabes Unidos")]
        EmiradosArabesUnidos,
        [Description("Equador")]
        Equador,
        [Description("Eritréia")]
        Eritreia,
        [Description("Escócia")]
        Escocia,
        [Description("Eslováquia")]
        Eslovaquia,
        [Description("Eslovênia")]
        Eslovenia,
        [Description("Espanha")]
        Espanha,
        [Description("Estados Unidos")]
        EstadosUnidos,
        [Description("Estônia")]
        Estonia,
        [Description("Etiópia")]
        Etiopia,
        [Description("Federação Russa")]
        FederaçãoRussa,
        [Description("Fiji")]
        Fiji,
        [Description("Filipinas")]
        Filipinas,
        [Description("Finlândia")]
        Finlandia,
        [Description("Formosa (Taiwan)")]
        Taiwan,
        [Description("França")]
        Franca,
        [Description("Gabão")]
        Gabao,
        [Description("Gâmbia")]
        Gambia,
        [Description("Gana")]
        Gana,
        [Description("Geórgia")]
        Georgia,
        [Description("Grã-Bretanha")]
        GraBretanha,
        [Description("Granada")]
        Granada,
        [Description("Grécia")]
        Grecia,
        [Description("Groenlândia")]
        Groenlandia,
        [Description("Guatemala")]
        Guatemala,
        [Description("Guiana")]
        Guiana,
        [Description("Guiana Francesa")]
        GuianaFrancesa,
        [Description("Guiné")]
        Guine,
        [Description("Guiné Bissau")]
        GuineBissau,
        [Description("Guiné Equatorial")]
        GuineEquatorial,
        [Description("Haiti")]
        Haiti,
        [Description("Holanda")]
        Holanda,
        [Description("Honduras")]
        Honduras,
        [Description("Hungria")]
        Hungria,
        [Description("Iêmen")]
        Iemen,
        [Description("Ilhas Marshall")]
        IlhasMarshall,
        [Description("Ilhas Salomão")]
        IlhasSalomao,
        [Description("Índia")]
        India,
        [Description("Indonésia")]
        Indonesia,
        [Description("Irã")]
        Ira,
        [Description("Iraque")]
        Iraque,
        [Description("Irlanda")]
        Irlanda,
        [Description(" Irlanda do Norte")]
        IrlandadoNorte,
        [Description("Islândia")]
        Islandia,
        [Description("Israel")]
        Israel,
        [Description("Itália")]
        Italia,
        [Description("Jamaica")]
        Jamaica,
        [Description("Japão")]
        Japao,
        [Description("Jordânia")]
        Jordania,
        [Description("Kiribati")]
        Kiribati,
        [Description("Kuweit")]
        Kuweit,
        [Description("Laos")]
        Laos,
        [Description("Lesoto")]
        Lesoto,
        [Description("Letônia")]
        Letonia,
        [Description("Líbano")]
        Libano,
        [Description("Libéria")]
        Liberia,
        [Description("Líbia")]
        Líbia,
        [Description("Liechtenstein")]
        Liechtenstein,
        [Description("Lituânia")]
        Lituania,
        [Description("Luxemburgo")]
        Luxemburgo,
        [Description("Macedônia")]
        Macedonia,
        [Description("Madagascar")]
        Madagascar,
        [Description("Malásia")]
        Malasia,
        [Description("Malauí")]
        Malaui,
        [Description("Maldivas")]
        Maldivas,
        [Description("Mali")]
        Mali,
        [Description("Malta")]
        Malta,
        [Description("Marrocos")]
        Marrocos,
        [Description("Maurício")]
        Mauricio,
        [Description("Mauritânia")]
        Mauritania,
        [Description("México")]
        Mexico,
        [Description("Mianmar")]
        Mianmar,
        [Description("Micronésia")]
        Micronesia,
        [Description("Moçambique")]
        Mocambique,
        [Description("Moldávia")]
        Moldavia,
        [Description("Mônaco")]
        Monaco,
        [Description("Mongólia")]
        Mongolia,
        [Description("Namíbia")]
        Namibia,
        [Description("Nauru")]
        Nauru,
        [Description("Nepal")]
        Nepal,
        [Description("Nicarágua")]
        Nicaragua,
        [Description("Niger")]
        Niger,
        [Description("Nigéria")]
        Nigeria,
        [Description("Noruega")]
        Noruega,
        [Description("Nova Zelândia")]
        NovaZelandia,
        [Description("Omã")]
        Oma,
        [Description("Panamá")]
        Panama,
        [Description("Palau")]
        Palau,
        [Description("Papua Nova Guiné")]
        PapuaNovaGuine,
        [Description("Paquistão")]
        Paquistao,
        [Description("Paraguai")]
        Paraguai,
        [Description("Peru")]
        Peru,
        [Description("Polônia")]
        Polonia,
        [Description("Porto Rico")]
        PortoRico,
        [Description("Portugal")]
        Portugal,
        [Description("Quênia")]
        Quenia,
        [Description("Quirguistão")]
        Quirguistao,
        [Description("Reino Unido")]
        ReinoUnido,
        [Description("Rep. Centro-Africana")]
        RepCentroAfricana,
        [Description("Rep. Dominicana")]
        RepDominicana,
        [Description("República Tcheca")]
        RepublicaTcheca,
        [Description("Romênia")]
        Romenia,
        [Description("Ruanda")]
        Ruanda,
        [Description("Samoa")]
        Samoa,
        [Description("San Marino")]
        SanMarino,
        [Description("Santa Lúcia")]
        SantaLucia,
        [Description("São Cristóvão e Névis")]
        SaoCristovaoNevis,
        [Description("São Tomé e Príncipe")]
        SaoTomePrincipe,
        [Description("São Vicente e Granadinas")]
        SaoVicenteGranadinas,
        [Description("Seicheles")]
        Seicheles,
        [Description("Serra Leoa")]
        SerraLeoa,
        [Description("Sérvia e Montenegro")]
        ServiaMontenegro,
        [Description("Síria")]
        Siria,
        [Description("Somália")]
        Somalia,
        [Description("Sri Lanka")]
        SriLanka,
        [Description("Suazilândia")]
        Suazilandia,
        [Description("Sudão")]
        Sudao,
        [Description("Suécia")]
        Suecia,
        [Description("Suíça")]
        Suica,
        [Description("Suriname")]
        Suriname,
        [Description("Tadjiquistão")]
        Tadjiquistao,
        [Description("Tailândia")]
        Tailandia,
        [Description("Tanzânia")]
        Tanzania,
        [Description("Togo")]
        Togo,
        [Description("Tonga")]
        Tonga,
        [Description("Trinidad e Tobago")]
        TrinidadTobago,
        [Description("Tunísia")]
        Tunisia,
        [Description("Turcomenistão")]
        Turcomenistao,
        [Description("Turquia")]
        Turquia,
        [Description("Tuvalu")]
        Tuvalu,
        [Description("Ucrânia")]
        Ucrania,
        [Description("Uganda")]
        Uganda,
        [Description("Uruguai")]
        Uruguai,
        [Description("Uzbequistão")]
        Uzbequistao,
        [Description("Vanuatu")]
        Vanuatu,
        [Description("Vaticano")]
        Vaticano,
        [Description("Venezuela")]
        Venezuela,
        [Description("Vietnã")]
        Vietna,
        [Description("Zaire")]
        Zaire,
        [Description("Zâmbia")]
        Zambia,
        [Description("Zimbábue")]
        Zimbabue
    }
}
