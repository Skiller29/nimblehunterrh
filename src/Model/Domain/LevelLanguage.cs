﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum LevelLanguage
    {
        [Description("Fluente")]
        Fluente = 0,
        [Description("Avançado")]
        Avancado,
        [Description("Intermediário")]
        Intermediario,
        [Description("Básico")]
        Basico
    }
}
