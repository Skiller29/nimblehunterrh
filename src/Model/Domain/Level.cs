﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum Level
    {
        [Description("Avançado")]
        Avancado = 0,
        [Description("Intermediário")]
        Intermediario,
        [Description("Básico")]
        Basico
    }
}
