﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain
{
    public enum StatusCurriculum
    {
        [Description("Ativo")]
        Ativo = 0,
        [Description("Inativo")]
        Inativo
    }
}
