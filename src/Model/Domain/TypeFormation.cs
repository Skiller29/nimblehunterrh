﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum TypeFormation
    {
        [Description("Ensino médio - Profissionalizante")]
        EnsinoMedio = 1,
        [Description("Graduação")]
        Graduacao,
        [Description("Pós-graduação")]
        PosGraduacao,
        [Description("Ensino fundamental")]
        EnsinoFundamental
    }
}
