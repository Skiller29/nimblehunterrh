﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum OccupationArea
    {
        [Description("Administrativa")]
        Administrativa = 0,
        [Description("Auditoria")]
        Auditoria,
        [Description("Bibliotecamia")]
        Bibliotecamia,
        [Description("Comercial")]
        Comercial,
        [Description("Comércio exterior")]
        ComercioExterior,
        [Description("Consultoria")]
        Consultoria,
        [Description("Contábil")]
        Contábil,
        [Description("Engenharia")]
        Engenharia,
        [Description("Financeira")]
        Financeira,
        [Description("Industrial/Produção")]
        Industrial,
        [Description("Informática")]
        Informatica,
        [Description("Instituição financeira")]
        InstituiçãoFinanceira,
        [Description("Instrumentação")]
        Instrumentação,
        [Description("Jurídica")]
        Jurídica,
        [Description("Marketing")]
        Marketing,
        [Description("Materiais")]
        Materiais,
        [Description("Recursos humanos")]
        RecursosHumanos,
        [Description("SAP")]
        SAP,
        [Description("Secretariado")]
        Secretariado,
        [Description("Outros")]
        Outros
    }
}
