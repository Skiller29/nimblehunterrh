﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum TypeActivity
    {
        [Description("Associações")]
        Associacoes = 0,
        [Description("Aulas particulares")]
        AulasParticulares,
        [Description("Cursos no exterior")]
        CursosExterior,
        [Description("Empresas Jr.")]
        EmpresasJr,
        [Description("Intercâmbio")]
        Intercambio,
        [Description("Participações em grêmios")]
        ParticipacoesGremios,
        [Description("Projetos")]
        Projetos,
        [Description("Voluntariado")]
        Voluntariado,
        [Description("Outros")]
        Outros
    }
}
