﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum DegreeKinship
    {
        [Description("Avô(ó)")]
        Avo = 0,
        [Description("Cônjuge")]
        Conjuge,
        [Description("Filho(a)")]
        Filho,
        [Description("Irmão(a)")]
        Irmao,
        [Description("Mãe")]
        Mae,
        [Description("Pai")]
        Pai,
        [Description("Primo(a)")]
        Primo,
        [Description("Tio(a)")]
        Tio,
        [Description("Outros")]
        Outros
    }
}
