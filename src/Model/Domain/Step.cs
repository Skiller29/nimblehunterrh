﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum Step
    {
        [Description("Entrevista")]
        Entrevista = 0,
        [Description("Avaliação")]
        Avaliacao,
        [Description("Triagem de currículos")]
        Triagem,
        [Description("Anúncio da vaga")]
        Anuncio
    }
}
