﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum MaritalStatus
    {
        [Description("Solteiro(a)")]
        Solteiro = 1,
        [Description("Casado(a)")]
        Casado,
        [Description("Divorciado(a)")]
        Divorciado,
        [Description("Viúvo(a)")]
        Viuvo
    }
}
