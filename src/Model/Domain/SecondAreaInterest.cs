﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum SecondAreaInterest
    {
        [Description("Apoio internacional")]
        ApoioInternacional = 0,
        [Description("Asset Manageament")]
        AssetManageament,
        [Description("Assuntos corporativos")]
        AssuntosCorporativos,
        [Description("Atacado e financiamentos")]
        AtacadoFinanciamentos,
        [Description("Auditoria")]
        Auditoria,
        [Description("Call center")]
        CallCenter,
        [Description("Cartões")]
        Cartoes,
        [Description("CFO")]
        CFO,
        [Description("Comunicação e marketing")]
        ComunicacaoMarketing,
        [Description("Corporais")]
        Corporais,
        [Description("Desenvolvimento novos negocios")]
        DesenvolvimentoNegocios,
        [Description("Financeira")]
        Financeira,
        [Description("Global Banking e Markets")]
        GlobalBanking,
        [Description("Juridico")]
        Jurídico,
        [Description("Negocios pessoa fisica (Produtos e segmentos)")]
        NegociosPessoaFisica,
        [Description("Negocios pessoa juridica (Produtos e segmentos)")]
        NegociosPessoaJuridica,
        [Description("Private")]
        Private,
        [Description("Recursos humanos")]
        RecursoHumanos,
        [Description("Rede comercial (Agencias)")]
        RedeComercial,
        [Description("Riscos credito e mercado")]
        RiscosCredito,
        [Description("Tecnologia da informação")]
        TecnologiaInformacao,
    }
}
