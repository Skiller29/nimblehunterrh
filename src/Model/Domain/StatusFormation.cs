﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum StatusFormation
    {
        [Description("Concluído")]
        Concluido = 1,
        [Description("Em andamento")]
        EmAndamento,
        [Description("Incompleto")]
        Incompleto,
        [Description("Trancado")]
        Trancado
    }
}
