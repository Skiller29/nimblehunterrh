using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TProfessionalExperience
    {
        public static TProfessionalExperience create(Int32 idCurriculum, String company, String occupation, OccupationArea occupationArea, String immediateSuperior, String companyPhone, Country country, Double lastSalary, String terminationReason, String actvitiesPerformed, DateTime admissionDate, DateTime terminatioDate, TCity city) 
        {
			return Service.create(idCurriculum, company, occupation, occupationArea, immediateSuperior, companyPhone, country, lastSalary, terminationReason, actvitiesPerformed, admissionDate, terminatioDate, city);
		}

        public static TProfessionalExperience Edit(Int32 idProfessionalExperience, String company, String occupation, OccupationArea occupationArea, String immediateSuperior, String companyPhone, Country country, Double lastSalary, String terminationReason, String actvitiesPerformed, DateTime admissionDate, DateTime terminatioDate, TCity city) 
        {
			return Service.Edit(idProfessionalExperience, company, occupation, occupationArea, immediateSuperior, companyPhone, country, lastSalary, terminationReason, actvitiesPerformed, admissionDate, terminatioDate, city);
		}

        public static TProfessionalExperience FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TProfessionalExperience Remove(Int32 idProfessionalExperience) 
        {
			return Service.Remove(idProfessionalExperience);
		}

        public static List<TCurriculum> SearchCurriculums(Nullable<OccupationArea> occupationArea, String occupation, String company, Nullable<Country> country, TCity city, String nameCityCountry) 
        {
			return Service.SearchCurriculums(occupationArea, occupation, company, country, city, nameCityCountry);
		}

    }
}