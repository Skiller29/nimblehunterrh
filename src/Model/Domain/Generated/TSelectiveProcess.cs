using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TSelectiveProcess : Entity<TSelectiveProcess, ITSelectiveProcessService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Observation { get; set; } 
        public virtual Boolean IsApproved { get; set; } 

        public virtual TCurriculum Curriculum { get; set; } 
        public virtual TStepsVacancy StepVacancy { get; set; } 


        #region ' Generated Helpers '
        static TSelectiveProcess()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TSelectiveProcess obj1, TSelectiveProcess obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TSelectiveProcess obj1, TSelectiveProcess obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TSelectiveProcess() 
        {
            Initialize();
        }
        
        public override TSelectiveProcess Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TSelectiveProcess(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}