using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TProfessionalCourse : Entity<TProfessionalCourse, ITProfessionalCourseService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Course { get; set; } 
        public virtual Country EnumCountry { get; set; } 
        public virtual String Institute { get; set; } 
        public virtual Int32? Workload { get; set; } 
        public virtual String NameCityCountry { get; set; } 
        public virtual DateTime? ConclusionDate { get; set; } 

        public virtual TCity City { get; set; } 
        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TProfessionalCourse()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TProfessionalCourse obj1, TProfessionalCourse obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TProfessionalCourse obj1, TProfessionalCourse obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TProfessionalCourse() 
        {
            Initialize();
        }
        
        public override TProfessionalCourse Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TProfessionalCourse(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}