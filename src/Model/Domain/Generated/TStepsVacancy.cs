using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TStepsVacancy : Entity<TStepsVacancy, ITStepsVacancyService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual Step EnumStep { get; set; } 
        public virtual DateTime InitialDate { get; set; } 
        public virtual DateTime EndTime { get; set; } 
        public virtual Status EnumStatus { get; set; } 

        public virtual TVacancy Vacancy { get; set; } 

        public virtual ICollection<TSelectiveProcess> TSelectiveProcesses { get; set; } 

        #region ' Generated Helpers '
        static TStepsVacancy()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TStepsVacancy obj1, TStepsVacancy obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TStepsVacancy obj1, TStepsVacancy obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TStepsVacancy() 
        {
            this.TSelectiveProcesses = new HashSet<TSelectiveProcess>();
            Initialize();
        }
        
        public override TStepsVacancy Clone()
        {
            var cloned = base.Clone();
            cloned.TSelectiveProcesses = null;
            return cloned;
        }

        public TStepsVacancy(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}