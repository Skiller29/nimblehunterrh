using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TAdditionalField : Entity<TAdditionalField, ITAdditionalFieldService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String NameField { get; set; } 
        public virtual TypeField EnumTypeField { get; set; } 

        public virtual TVacancy Vacancy { get; set; } 

        public virtual ICollection<TAnswersAdditionalField> TAnswersAdditionalFields { get; set; } 

        #region ' Generated Helpers '
        static TAdditionalField()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TAdditionalField obj1, TAdditionalField obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TAdditionalField obj1, TAdditionalField obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TAdditionalField() 
        {
            this.TAnswersAdditionalFields = new HashSet<TAnswersAdditionalField>();
            Initialize();
        }
        
        public override TAdditionalField Clone()
        {
            var cloned = base.Clone();
            cloned.TAnswersAdditionalFields = null;
            return cloned;
        }

        public TAdditionalField(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}