using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TKnowledgeInformatic
    {
        public static TKnowledgeInformatic create(Int32 idCurriculum, String software, Level level) 
        {
			return Service.create(idCurriculum, software, level);
		}

        public static TKnowledgeInformatic Edit(Int32 idKnowledgeInformaticy, String software, Level level) 
        {
			return Service.Edit(idKnowledgeInformaticy, software, level);
		}

        public static TKnowledgeInformatic FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TKnowledgeInformatic Remove(Int32 idKnowledgeInformaticy) 
        {
			return Service.Remove(idKnowledgeInformaticy);
		}

        public static List<TCurriculum> SearchCurriculums(String software, Nullable<Level> level) 
        {
			return Service.SearchCurriculums(software, level);
		}

    }
}