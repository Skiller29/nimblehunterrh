using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TKnowledgeInformatic : Entity<TKnowledgeInformatic, ITKnowledgeInformaticService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Software { get; set; } 
        public virtual Level EnumLevel { get; set; } 

        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TKnowledgeInformatic()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TKnowledgeInformatic obj1, TKnowledgeInformatic obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TKnowledgeInformatic obj1, TKnowledgeInformatic obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TKnowledgeInformatic() 
        {
            Initialize();
        }
        
        public override TKnowledgeInformatic Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TKnowledgeInformatic(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}