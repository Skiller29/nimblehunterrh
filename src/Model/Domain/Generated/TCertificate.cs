using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TCertificate : Entity<TCertificate, ITCertificateService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Certification { get; set; } 
        public virtual Int32 NumberOrganClass { get; set; } 
        public virtual String OrganIssuer { get; set; } 

        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TCertificate()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TCertificate obj1, TCertificate obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TCertificate obj1, TCertificate obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TCertificate() 
        {
            Initialize();
        }
        
        public override TCertificate Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TCertificate(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}