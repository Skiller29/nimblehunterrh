using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TUser
    {
        public static TUser create(String password, String cpf, Int32 idEnterprise, Profile profile) 
        {
			return Service.create(password, cpf, idEnterprise, profile);
		}

        public static TUser create(String password, String cpf) 
        {
			return Service.create(password, cpf);
		}

        public static TUser EditCpf(String cpf, String newCpf) 
        {
			return Service.EditCpf(cpf, newCpf);
		}

        public static TUser FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TUser FindByCpf(String cpf) 
        {
			return Service.FindByCpf(cpf);
		}

        public static Byte[] HashPassword(String password) 
        {
			return Service.HashPassword(password);
		}

        public static TUser Authenticate(String cpf, String password) 
        {
			return Service.Authenticate(cpf, password);
		}

        public static TUser Remove(Int32 idUser) 
        {
			return Service.Remove(idUser);
		}

        public static List<TUser> ListAll() 
        {
			return Service.ListAll();
		}

        public static List<TCurriculum> SearchCurriculums(String name, Nullable<Genre> genre, Nullable<MaritalStatus> maritalStatus, TCity city) 
        {
			return Service.SearchCurriculums(name, genre, maritalStatus, city);
		}

    }
}