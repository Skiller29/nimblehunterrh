using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TExtraActivity : Entity<TExtraActivity, ITExtraActivityService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual TypeActivity EnumTypeActivity { get; set; } 
        public virtual DateTime InitialDate { get; set; } 
        public virtual DateTime? EndDate { get; set; } 
        public virtual Country EnumCountry { get; set; } 
        public virtual String Description { get; set; } 
        public virtual String NameCityCountry { get; set; } 

        public virtual TCity City { get; set; } 
        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TExtraActivity()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TExtraActivity obj1, TExtraActivity obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TExtraActivity obj1, TExtraActivity obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TExtraActivity() 
        {
            Initialize();
        }
        
        public override TExtraActivity Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TExtraActivity(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}