using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TUser : Entity<TUser, ITUserService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual DateTime CreationDate { get; set; } 
        public virtual String Cpf { get; set; } 
        public virtual Byte[] Password { get; set; } 
        public virtual Profile EnumProfile { get; set; } 
        public virtual String FullName { get; set; } 
        public virtual DateTime? BirthDate { get; set; } 
        public virtual String Nationality { get; set; } 
        public virtual Genre? EnumGenre { get; set; } 
        public virtual MaritalStatus? EnumMaritalStatus { get; set; } 
        public virtual Boolean IsDeficient { get; set; } 
        public virtual String Address { get; set; } 
        public virtual String Cep { get; set; } 
        public virtual String Number { get; set; } 
        public virtual String Complement { get; set; } 
        public virtual String Neighborhood { get; set; } 
        public virtual String Email { get; set; } 
        public virtual String HomePhone { get; set; } 
        public virtual String CellPhone { get; set; } 
        public virtual String OptionalPhone { get; set; } 
        public virtual String UrlPhoto { get; set; } 
        public virtual String Token { get; set; } 

        public virtual TCity City { get; set; } 
        public virtual TEnterprise Enterprise { get; set; } 

        public virtual ICollection<TCurriculum> TCurriculums { get; set; } 

        #region ' Generated Helpers '
        static TUser()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TUser obj1, TUser obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TUser obj1, TUser obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TUser() 
        {
            this.TCurriculums = new HashSet<TCurriculum>();
            Initialize();
        }
        
        public override TUser Clone()
        {
            var cloned = base.Clone();
            cloned.TCurriculums = null;
            return cloned;
        }

        public TUser(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}