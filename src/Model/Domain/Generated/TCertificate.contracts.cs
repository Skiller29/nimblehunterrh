using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TCertificate
    {
        public static TCertificate create(Int32 idCurriculum, String certification, Int32 numberOrganClass, String organIssuer) 
        {
			return Service.create(idCurriculum, certification, numberOrganClass, organIssuer);
		}

        public static TCertificate Edit(Int32 idCertificate, String certification, Int32 numberOrganClass, String organIssuer) 
        {
			return Service.Edit(idCertificate, certification, numberOrganClass, organIssuer);
		}

        public static TCertificate FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TCertificate Remove(Int32 idCertificate) 
        {
			return Service.Remove(idCertificate);
		}

        public static List<TCurriculum> SearchCurriculums(String certificate, String organ, Int32 number) 
        {
			return Service.SearchCurriculums(certificate, organ, number);
		}

    }
}