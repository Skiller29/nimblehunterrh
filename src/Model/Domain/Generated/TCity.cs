using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TCity : Entity<TCity, ITCityService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String CityName { get; set; } 
        public virtual Boolean Capital { get; set; } 

        public virtual TState State { get; set; } 

        public virtual ICollection<TAcademicFormation> TAcademicFormations { get; set; } 
        public virtual ICollection<TEnterprise> TEnterprises { get; set; } 
        public virtual ICollection<TExtraActivity> TExtraActivities { get; set; } 
        public virtual ICollection<TProfessionalCourse> TProfessionalCourses { get; set; } 
        public virtual ICollection<TProfessionalExperience> TProfessionalExperiences { get; set; } 
        public virtual ICollection<TUser> TUsers { get; set; } 
        public virtual ICollection<TVacancy> TVacancies { get; set; } 

        #region ' Generated Helpers '
        static TCity()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TCity obj1, TCity obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TCity obj1, TCity obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TCity() 
        {
            this.TAcademicFormations = new HashSet<TAcademicFormation>();
            this.TEnterprises = new HashSet<TEnterprise>();
            this.TExtraActivities = new HashSet<TExtraActivity>();
            this.TProfessionalCourses = new HashSet<TProfessionalCourse>();
            this.TProfessionalExperiences = new HashSet<TProfessionalExperience>();
            this.TUsers = new HashSet<TUser>();
            this.TVacancies = new HashSet<TVacancy>();
            Initialize();
        }
        
        public override TCity Clone()
        {
            var cloned = base.Clone();
            cloned.TAcademicFormations = null;
            cloned.TEnterprises = null;
            cloned.TExtraActivities = null;
            cloned.TProfessionalCourses = null;
            cloned.TProfessionalExperiences = null;
            cloned.TUsers = null;
            cloned.TVacancies = null;
            return cloned;
        }

        public TCity(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}