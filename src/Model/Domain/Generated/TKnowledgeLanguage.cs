using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TKnowledgeLanguage : Entity<TKnowledgeLanguage, ITKnowledgeLanguageService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual Language EnumLanguage { get; set; } 
        public virtual LevelLanguage EnumConversation { get; set; } 
        public virtual LevelLanguage EnumReading { get; set; } 
        public virtual LevelLanguage EnumWriting { get; set; } 

        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TKnowledgeLanguage()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TKnowledgeLanguage obj1, TKnowledgeLanguage obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TKnowledgeLanguage obj1, TKnowledgeLanguage obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TKnowledgeLanguage() 
        {
            Initialize();
        }
        
        public override TKnowledgeLanguage Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TKnowledgeLanguage(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}