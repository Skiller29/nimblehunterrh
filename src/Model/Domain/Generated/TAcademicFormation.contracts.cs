using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TAcademicFormation
    {
        public static TAcademicFormation create(Int32 idCurriculum, TypeFormation typeFormation, Country country, String course, String institute, StatusFormation statusFormation, DateTime initialDate, DateTime endDate) 
        {
			return Service.create(idCurriculum, typeFormation, country, course, institute, statusFormation, initialDate, endDate);
		}

        public static TAcademicFormation Edit(Int32 idAcademicFormation, TypeFormation typeFormation, Country country, String course, String institute, StatusFormation statusFormation, DateTime initialDate, DateTime endDate) 
        {
			return Service.Edit(idAcademicFormation, typeFormation, country, course, institute, statusFormation, initialDate, endDate);
		}

        public static TAcademicFormation FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TAcademicFormation Remove(Int32 idAcademicFormation) 
        {
			return Service.Remove(idAcademicFormation);
		}

        public static List<TCurriculum> SearchCurriculums(Nullable<StatusFormation> statusFormation, Nullable<TypeFormation> typeFormation, String course, String institute, Nullable<Country> country, TCity city, String nameCityCountry) 
        {
			return Service.SearchCurriculums(statusFormation, typeFormation, course, institute, country, city, nameCityCountry);
		}

    }
}