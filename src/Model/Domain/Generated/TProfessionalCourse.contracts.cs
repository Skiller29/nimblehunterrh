using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TProfessionalCourse
    {
        public static TProfessionalCourse create(Int32 idCurriculum, DateTime conclusionDate, TCity city, Country country, String course, String institute, Int32 workload) 
        {
			return Service.create(idCurriculum, conclusionDate, city, country, course, institute, workload);
		}

        public static TProfessionalCourse Edit(Int32 idProfessionalCourse, DateTime conclusionDate, TCity city, Country country, String course, String institute, Int32 workload) 
        {
			return Service.Edit(idProfessionalCourse, conclusionDate, city, country, course, institute, workload);
		}

        public static TProfessionalCourse FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TProfessionalCourse Remove(Int32 idProfessionalCourse) 
        {
			return Service.Remove(idProfessionalCourse);
		}

        public static List<TCurriculum> SearchCurriculums(String course, String institute, Nullable<Country> country, TCity city, String nameCityCountry) 
        {
			return Service.SearchCurriculums(course, institute, country, city, nameCityCountry);
		}

    }
}