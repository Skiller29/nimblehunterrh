using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TVacancy : Entity<TVacancy, ITVacancyService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Ocupation { get; set; } 
        public virtual Boolean IsVacancyDeficient { get; set; } 
        public virtual Boolean IsVisibleCandidate { get; set; } 
        public virtual String Description { get; set; } 
        public virtual Status EnumStatus { get; set; } 

        public virtual TCity City { get; set; } 
        public virtual TEnterprise Enterprise { get; set; } 

        public virtual ICollection<TAdditionalField> TAdditionalFields { get; set; } 
        public virtual ICollection<TStepsVacancy> TStepsVacancies { get; set; } 

        #region ' Generated Helpers '
        static TVacancy()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TVacancy obj1, TVacancy obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TVacancy obj1, TVacancy obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TVacancy() 
        {
            this.TAdditionalFields = new HashSet<TAdditionalField>();
            this.TStepsVacancies = new HashSet<TStepsVacancy>();
            Initialize();
        }
        
        public override TVacancy Clone()
        {
            var cloned = base.Clone();
            cloned.TAdditionalFields = null;
            cloned.TStepsVacancies = null;
            return cloned;
        }

        public TVacancy(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}