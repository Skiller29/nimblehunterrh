using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TEnterprise
    {
        public static TEnterprise create(TCity city, String cnpj, String name, String cep, String phone, LineBusiness lineBusiness, Boolean isMultinacional, String logoUrl, String neighbhorood, String companyWebsite) 
        {
			return Service.create(city, cnpj, name, cep, phone, lineBusiness, isMultinacional, logoUrl, neighbhorood, companyWebsite);
		}

        public static TEnterprise Edit(Int32 idEnterprise, TCity city, String cnpj, String name, String cep, String phone, LineBusiness lineBusiness, Boolean isMultinacional, String logoUrl, String neighbhorood, String companyWebsite) 
        {
			return Service.Edit(idEnterprise, city, cnpj, name, cep, phone, lineBusiness, isMultinacional, logoUrl, neighbhorood, companyWebsite);
		}

        public static TEnterprise FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TEnterprise Remove(Int32 idEnterprise) 
        {
			return Service.Remove(idEnterprise);
		}

        public static List<TEnterprise> ListAll() 
        {
			return Service.ListAll();
		}

    }
}