using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TKnowledgeLanguage
    {
        public static TKnowledgeLanguage create(Int32 idCurriculum, Language language, LevelLanguage reading, LevelLanguage writing, LevelLanguage conversation) 
        {
			return Service.create(idCurriculum, language, reading, writing, conversation);
		}

        public static TKnowledgeLanguage Edit(Int32 idKnowledgeLanguage, Language language, LevelLanguage reading, LevelLanguage writing, LevelLanguage conversation) 
        {
			return Service.Edit(idKnowledgeLanguage, language, reading, writing, conversation);
		}

        public static TKnowledgeLanguage FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TKnowledgeLanguage Remove(Int32 idKnowledgeLanguage) 
        {
			return Service.Remove(idKnowledgeLanguage);
		}

        public static List<TCurriculum> SearchCurriculums(Nullable<Language> language, Nullable<LevelLanguage> conversation, Nullable<LevelLanguage> writing, Nullable<LevelLanguage> reading) 
        {
			return Service.SearchCurriculums(language, conversation, writing, reading);
		}

    }
}