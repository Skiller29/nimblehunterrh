using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;

namespace NimbleHunterRH.Domain
{
    public partial class TVacancy
    {
        public static TVacancy create(TCity city, String occupation, Int32 numberJobs, Boolean isVacancyDeficient, Boolean isVisibleCandidate, String description, TEnterprise enterprise) 
        {
			return Service.create(city, occupation, numberJobs, isVacancyDeficient, isVisibleCandidate, description, enterprise);
		}

        public static TVacancy Edit(Int32 idVacancy, TCity city, String occupation, Int32 numberJobs, Boolean isVacancyDeficient, Boolean isVisibleCandidate, String description) 
        {
			return Service.Edit(idVacancy, city, occupation, numberJobs, isVacancyDeficient, isVisibleCandidate, description);
		}

        public static TVacancy FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TVacancy Remove(Int32 idVacancy) 
        {
			return Service.Remove(idVacancy);
		}

    }
}