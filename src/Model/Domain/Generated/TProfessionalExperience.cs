using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TProfessionalExperience : Entity<TProfessionalExperience, ITProfessionalExperienceService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Company { get; set; } 
        public virtual String Occupation { get; set; } 
        public virtual OccupationArea EnumOccupationArea { get; set; } 
        public virtual DateTime AdmissionDate { get; set; } 
        public virtual DateTime TerminationDate { get; set; } 
        public virtual String NameCityCountry { get; set; } 
        public virtual String ImmediateSuperior { get; set; } 
        public virtual String CompanyPhone { get; set; } 
        public virtual Country EnumCountry { get; set; } 
        public virtual Double? LastSalary { get; set; } 
        public virtual String TerminationReason { get; set; } 
        public virtual String ActivitiesPerformed { get; set; } 

        public virtual TCity City { get; set; } 
        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TProfessionalExperience()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TProfessionalExperience obj1, TProfessionalExperience obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TProfessionalExperience obj1, TProfessionalExperience obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TProfessionalExperience() 
        {
            Initialize();
        }
        
        public override TProfessionalExperience Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TProfessionalExperience(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}