using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TCurriculum
    {
        public static TCurriculum create(Int32 idUser, Int32 idEnterprise, DateTime admitionDateWork, DegreeKinship degreeKinship, StatusCurriculum statusCurriculum, Boolean hasKinshipWorked, String nameKinship, String phoneKinship, String sectorKinship, String sectorWorked, String terminationReasonWork, DateTime terminationDateWork, String photoUrl, Boolean workedCompany) 
        {
			return Service.create(idUser, idEnterprise, admitionDateWork, degreeKinship, statusCurriculum, hasKinshipWorked, nameKinship, phoneKinship, sectorKinship, sectorWorked, terminationReasonWork, terminationDateWork, photoUrl, workedCompany);
		}

        public static TCurriculum Edit(Int32 idCurriculum, DateTime admitionDateWork, DegreeKinship degreeKinship, StatusCurriculum statusCurriculum, Boolean hasKinshipWorked, String nameKinship, String phoneKinship, String sectorKinship, String sectorWorked, String terminationReasonWork, DateTime terminationDateWork, String photoUrl, Boolean workedCompany) 
        {
			return Service.Edit(idCurriculum, admitionDateWork, degreeKinship, statusCurriculum, hasKinshipWorked, nameKinship, phoneKinship, sectorKinship, sectorWorked, terminationReasonWork, terminationDateWork, photoUrl, workedCompany);
		}

        public static TCurriculum FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TCurriculum Remove(Int32 idCurriculum) 
        {
			return Service.Remove(idCurriculum);
		}

        public static List<TCurriculum> SearchCurrilums(Nullable<AreaInterest> firstAreaInterest, Nullable<AreaInterest> secondAreaInterest, Nullable<AreaInterest> thiAreaInterest, Nullable<SalaryRequeriments> salary) 
        {
			return Service.SearchCurrilums(firstAreaInterest, secondAreaInterest, thiAreaInterest, salary);
		}

    }
}