using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TState : Entity<TState, ITStateService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String StateName { get; set; } 
        public virtual String Sigla { get; set; } 


        public virtual ICollection<TCity> TCities { get; set; } 

        #region ' Generated Helpers '
        static TState()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TState obj1, TState obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TState obj1, TState obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TState() 
        {
            this.TCities = new HashSet<TCity>();
            Initialize();
        }
        
        public override TState Clone()
        {
            var cloned = base.Clone();
            cloned.TCities = null;
            return cloned;
        }

        public TState(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}