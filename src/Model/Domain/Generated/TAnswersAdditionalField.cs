using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TAnswersAdditionalField : Entity<TAnswersAdditionalField, ITAnswersAdditionalFieldService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Answer { get; set; } 

        public virtual TAdditionalField AdditionalField { get; set; } 
        public virtual TCurriculum Curriculum { get; set; } 


        #region ' Generated Helpers '
        static TAnswersAdditionalField()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TAnswersAdditionalField obj1, TAnswersAdditionalField obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TAnswersAdditionalField obj1, TAnswersAdditionalField obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TAnswersAdditionalField() 
        {
            Initialize();
        }
        
        public override TAnswersAdditionalField Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TAnswersAdditionalField(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}