using Simple.Entities;
using NimbleHunterRH.Domain;
using Simple.Services;
using NimbleHunterRH.Services;
using System;
using System.Collections.Generic;

namespace NimbleHunterRH.Domain
{
    public partial class TExtraActivity
    {
        public static TExtraActivity create(Int32 idCurriculum, TypeActivity typeActivity, DateTime initialDate, DateTime endDate, Country country, String description, TCity city) 
        {
			return Service.create(idCurriculum, typeActivity, initialDate, endDate, country, description, city);
		}

        public static TExtraActivity Edit(Int32 idExtraActivity, TypeActivity typeActivity, DateTime initialDate, DateTime endDate, Country country, String description, TCity city) 
        {
			return Service.Edit(idExtraActivity, typeActivity, initialDate, endDate, country, description, city);
		}

        public static TExtraActivity FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TExtraActivity Remove(Int32 idExtraActivity) 
        {
			return Service.Remove(idExtraActivity);
		}

        public static List<TCurriculum> SearchCurriculums(Nullable<TypeActivity> typeActivity, Nullable<Country> country, TCity city, String nameCityCountry) 
        {
			return Service.SearchCurriculums(typeActivity, country, city, nameCityCountry);
		}

    }
}