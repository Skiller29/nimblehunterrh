using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TEnterprise : Entity<TEnterprise, ITEnterpriseService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual DateTime CreationDate { get; set; } 
        public virtual String Name { get; set; } 
        public virtual String Cnpj { get; set; } 
        public virtual String Token { get; set; } 
        public virtual String Cep { get; set; } 
        public virtual String Neighbohood { get; set; } 
        public virtual String CompanyWebsite { get; set; } 
        public virtual Boolean IsMultinacional { get; set; } 
        public virtual String Phone { get; set; } 
        public virtual LineBusiness EnumLineBusiness { get; set; } 

        public virtual TCity City { get; set; } 

        public virtual ICollection<TCurriculum> TCurriculums { get; set; } 
        public virtual ICollection<TUser> TUsers { get; set; } 
        public virtual ICollection<TVacancy> TVacancies { get; set; } 

        #region ' Generated Helpers '
        static TEnterprise()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TEnterprise obj1, TEnterprise obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TEnterprise obj1, TEnterprise obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TEnterprise() 
        {
            this.TCurriculums = new HashSet<TCurriculum>();
            this.TUsers = new HashSet<TUser>();
            this.TVacancies = new HashSet<TVacancy>();
            Initialize();
        }
        
        public override TEnterprise Clone()
        {
            var cloned = base.Clone();
            cloned.TCurriculums = null;
            cloned.TUsers = null;
            cloned.TVacancies = null;
            return cloned;
        }

        public TEnterprise(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}