using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using NimbleHunterRH.Services;

namespace NimbleHunterRH.Domain
{
    [Serializable]
    public partial class TCurriculum : Entity<TCurriculum, ITCurriculumService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual AreaInterest EnumFirstAreaInterest { get; set; } 
        public virtual AreaInterest? EnumSecondAreaInterest { get; set; } 
        public virtual AreaInterest? EnumThirdAreaInterest { get; set; } 
        public virtual SalaryRequeriments? EnumSalaryRequeriments { get; set; } 
        public virtual Boolean WorkedCompany { get; set; } 
        public virtual String SectorWorked { get; set; } 
        public virtual DateTime? AdmitionDateWork { get; set; } 
        public virtual DateTime? TerminationDateWork { get; set; } 
        public virtual String TerminationReasonWork { get; set; } 
        public virtual Boolean HasKinshipWorked { get; set; } 
        public virtual DegreeKinship? EnumDegreeKinship { get; set; } 
        public virtual String NameKinship { get; set; } 
        public virtual String SectorKinship { get; set; } 
        public virtual String PhoneKinship { get; set; } 
        public virtual DateTime LastModification { get; set; } 
        public virtual DateTime CreationDate { get; set; } 
        public virtual StatusCurriculum EnumStatusCurriculum { get; set; } 

        public virtual TEnterprise Entreprise { get; set; } 
        public virtual TUser User { get; set; } 

        public virtual ICollection<TAcademicFormation> TAcademicFormations { get; set; } 
        public virtual ICollection<TAnswersAdditionalField> TAnswersAdditionalFields { get; set; } 
        public virtual ICollection<TCertificate> TCertificates { get; set; } 
        public virtual ICollection<TExtraActivity> TExtraActivities { get; set; } 
        public virtual ICollection<TKnowledgeInformatic> TKnowledgeInformatics { get; set; } 
        public virtual ICollection<TKnowledgeLanguage> TKnowledgeLanguages { get; set; } 
        public virtual ICollection<TProfessionalCourse> TProfessionalCourses { get; set; } 
        public virtual ICollection<TProfessionalExperience> TProfessionalExperiences { get; set; } 
        public virtual ICollection<TSelectiveProcess> TSelectiveProcesses { get; set; } 

        #region ' Generated Helpers '
        static TCurriculum()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TCurriculum obj1, TCurriculum obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TCurriculum obj1, TCurriculum obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TCurriculum() 
        {
            this.TAcademicFormations = new HashSet<TAcademicFormation>();
            this.TAnswersAdditionalFields = new HashSet<TAnswersAdditionalField>();
            this.TCertificates = new HashSet<TCertificate>();
            this.TExtraActivities = new HashSet<TExtraActivity>();
            this.TKnowledgeInformatics = new HashSet<TKnowledgeInformatic>();
            this.TKnowledgeLanguages = new HashSet<TKnowledgeLanguage>();
            this.TProfessionalCourses = new HashSet<TProfessionalCourse>();
            this.TProfessionalExperiences = new HashSet<TProfessionalExperience>();
            this.TSelectiveProcesses = new HashSet<TSelectiveProcess>();
            Initialize();
        }
        
        public override TCurriculum Clone()
        {
            var cloned = base.Clone();
            cloned.TAcademicFormations = null;
            cloned.TAnswersAdditionalFields = null;
            cloned.TCertificates = null;
            cloned.TExtraActivities = null;
            cloned.TKnowledgeInformatics = null;
            cloned.TKnowledgeLanguages = null;
            cloned.TProfessionalCourses = null;
            cloned.TProfessionalExperiences = null;
            cloned.TSelectiveProcesses = null;
            return cloned;
        }

        public TCurriculum(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}