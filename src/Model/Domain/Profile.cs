﻿using System.ComponentModel;

namespace NimbleHunterRH.Domain
{
    public enum Profile
    {
        [Description("Candidato")]
        Candidato = 0,
        [Description("Administrador da empresa")]
        AdminEmpresa,
        [Description("Administrador do sistema")]
        AdminSistema
    }
}
