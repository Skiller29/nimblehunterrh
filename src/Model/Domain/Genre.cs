﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain
{
    public enum Genre
    {
        [Description("Masculino")]
        Masculino = 1,
        [Description("Feminino")]
        Feminino
    }
}
