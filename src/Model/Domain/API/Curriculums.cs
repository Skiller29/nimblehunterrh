﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Domain.API;

namespace NimbleHunterRH.Domain
{
     public class Curriculums
    {
        public List<Curriculum> CurriculumsList { get; set; }
        public String Error { get; set; }

        public Curriculums(List<TCurriculum> curriculums)
        {
            this.CurriculumsList = new List<Curriculum>();
            foreach (var curriculum in curriculums)
            {
                this.CurriculumsList.Add(new Curriculum(curriculum));
            }
        }

        public Curriculums() { }
    }
}
