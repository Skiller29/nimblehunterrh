﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;


namespace NimbleHunterRH.Domain.API
{
    public class KnowledgeLanguage
    {
        public Int32 Id { get; set; }

        public String EnumLanguage { get; set; }
        public String EnumConversation { get; set; }
        public String EnumReading { get; set; }
        public String EnumWriting { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public KnowledgeLanguage(TKnowledgeLanguage knowledgeLanguage)
        {
            this.Id = knowledgeLanguage.Id;
            this.EnumConversation = knowledgeLanguage.EnumConversation.Description();
            this.EnumLanguage = knowledgeLanguage.EnumLanguage.Description();
            this.EnumReading = knowledgeLanguage.EnumReading.Description();
            this.EnumWriting = knowledgeLanguage.EnumWriting.Description();
        }

        public KnowledgeLanguage() { }
    }
}
