﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class SelectiveProcesses
    {
        public List<SelectiveProcess> Processes { get; set; }
        public String Error { get; set; }

        public SelectiveProcesses(List<TSelectiveProcess> processes)
        {
            this.Processes = new List<SelectiveProcess>();
            foreach (var process in processes)
            {
                this.Processes.Add(new SelectiveProcess(process));
            }
        }

        public SelectiveProcesses() { }
    }
}
