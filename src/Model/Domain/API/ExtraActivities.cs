﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
     public class ExtraActivities
    {
        public List<ExtraActivity> Activites { get; set; }
        public String Error { get; set; }

        public ExtraActivities(List<TExtraActivity> activities)
        {
            this.Activites = new List<ExtraActivity>();
            foreach (var activity in activities)
            {
                this.Activites.Add(new ExtraActivity(activity));
            }
        }

        public ExtraActivities() { }
    }
}
