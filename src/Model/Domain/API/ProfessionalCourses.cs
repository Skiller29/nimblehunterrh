﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class ProfessionalCourses
    {
            public List<ProfessionalCourse> Courses { get; set; }
            public String Error { get; set; }

            public ProfessionalCourses(List<TProfessionalCourse> courses)
            {
                this.Courses = new List<ProfessionalCourse>();
                foreach (var course in courses)
                {
                    this.Courses.Add(new ProfessionalCourse(course));
                }
            }

            public ProfessionalCourses() { }
        }
}
