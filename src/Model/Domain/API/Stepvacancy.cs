﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class Stepvacancy
    {
        public Int32 Id { get; set; }

        public String EnumStep { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime EndTime { get; set; }
        public String EnumStatus { get; set; }
        public Int32 IdVacancy { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public Stepvacancy(TStepsVacancy step)
        {
            this.Id = step.Id;
            this.EnumStep = step.EnumStep.Description();
            this.InitialDate = step.InitialDate;
            this.EndTime = step.EndTime;
            this.EnumStatus = step.EnumStatus.Description();
            this.IdVacancy = step.Vacancy.Id;
        }

        public Stepvacancy() { }
    }
}
