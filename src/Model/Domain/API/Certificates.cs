﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class Certificates
    {
        public List<Certificate> CertificatesList { get; set; }
        public String Error { get; set; }

        public Certificates(List<TCertificate> certificates)
        {
            this.CertificatesList = new List<Certificate>();
            foreach (var certificate in certificates)
            {
                this.CertificatesList.Add(new Certificate(certificate));
            }
        }

        public Certificates() { }
    }
}
