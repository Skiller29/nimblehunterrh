﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class StepsVacancy
    {
        public List<Stepvacancy> Steps { get; set; }
        public String Error { get; set; }


        public StepsVacancy(List<TStepsVacancy> steps)
        {
            this.Steps = new List<Stepvacancy>();
            foreach (var step in steps)
            {
                this.Steps.Add(new Stepvacancy(step));
            }
        }

        public StepsVacancy() { }
    }
}
