﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class SelectiveProcess
    {
        public Int32 Id { get; set; }

        public String Observation { get; set; }
        public Boolean IsApproved { get; set; }
        public String Candidate { get; set; }
        public String Vacancy { get; set; }
        public String StatusVacancy { get; set; }
        public int IdUser { get; set; }
        public int IdStep { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public SelectiveProcess(TSelectiveProcess selectiveProcess)
        {
            this.Id = selectiveProcess.Id;
            this.Observation = selectiveProcess.Observation;
            this.IsApproved = selectiveProcess.IsApproved;
            this.Candidate = selectiveProcess.Curriculum.User.FullName;
            this.Vacancy = selectiveProcess.StepVacancy.Vacancy.Ocupation;
            this.StatusVacancy = selectiveProcess.StepVacancy.Vacancy.EnumStatus.Description();
            this.IdUser = selectiveProcess.Curriculum.User.Id;
            this.IdStep = selectiveProcess.StepVacancy.Id;
        }

        public SelectiveProcess() { }
    }
}
