﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class Enterprise
    {
        public virtual Int32 Id { get; set; }

        public DateTime CreationDate { get; set; }
        public String Name { get; set; }
        public String Cnpj { get; set; }
        public String Token { get; set; }
        public String Cep { get; set; }
        public String Neighbohood { get; set; }
        public String CompanyWebsite { get; set; }
        public Boolean IsMultinacional { get; set; }
        public String Phone { get; set; }
        public String EnumLineBusiness { get; set; }

        public String Error { get; set; }

        public Enterprise(TEnterprise enterprise)
        {
            this.Id = enterprise.Id;
            this.CreationDate = enterprise.CreationDate;
            this.Name = enterprise.Name;
            this.Cnpj = enterprise.Cnpj;
            this.Token = enterprise.Token;
            this.Cep = enterprise.Cep;
            this.Neighbohood = enterprise.Neighbohood;
            this.CompanyWebsite = enterprise.CompanyWebsite;
            this.IsMultinacional = enterprise.IsMultinacional;
            this.Phone = enterprise.Phone;
            this.EnumLineBusiness = enterprise.EnumLineBusiness.ToString();
        }

        public Enterprise() { }
    }
}
