﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class AcademicFormations
    {
        public List<AcademicFormation> Formations { get; set; }
        public String Error { get; set; }

        public AcademicFormations(List<TAcademicFormation> formations)
        {
            this.Formations = new List<AcademicFormation>();
            foreach (var formation in formations)
            {
                this.Formations.Add(new AcademicFormation(formation));
            }
        }

        public AcademicFormations() { }
    }
}
