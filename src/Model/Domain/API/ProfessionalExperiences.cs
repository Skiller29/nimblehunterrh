﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class ProfessionalExperiences
    {
        public List<ProfessionalExperience> Experiences { get; set; }
        public String Error { get; set; }

        public ProfessionalExperiences(List<TProfessionalExperience> experiences)
        {
            this.Experiences = new List<ProfessionalExperience>();
            foreach (var experience in experiences)
            {
                this.Experiences.Add(new ProfessionalExperience(experience));
            }
        }

        public ProfessionalExperiences() { }
    }
}
