﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Visitors;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class Vacancy
    {
        public virtual Int32 Id { get; set; }

        public String Ocupation { get; set; }
        public Boolean IsVacancyDeficient { get; set; }
        public Boolean IsVisibleCandidate { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public Boolean HasFields { get; set; }
        public Boolean HasSteps { get; set; }
        public int IdCurrentStep { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public Vacancy(TVacancy vacancy)
        {
            this.Id = vacancy.Id;
            this.Ocupation = vacancy.Ocupation;
            this.IsVacancyDeficient = vacancy.IsVacancyDeficient;
            this.IsVisibleCandidate = vacancy.IsVisibleCandidate;
            this.Description = vacancy.Description;
            this.Status = vacancy.EnumStatus.Description();
            this.City = vacancy.City.CityName;
            this.State = vacancy.City.State.Sigla;
            this.HasFields = vacancy.TAdditionalFields.Any();
            this.HasSteps = vacancy.TStepsVacancies.Any();
            var firstOrDefault = vacancy.TStepsVacancies.FirstOrDefault(x => x.EnumStatus == Domain.Status.EmAndamento);
            if (firstOrDefault != null && firstOrDefault.Id != 0)
                this.IdCurrentStep = firstOrDefault.Id;
            else
                this.IdCurrentStep = vacancy.TStepsVacancies.Any() ? vacancy.TStepsVacancies.FirstOrDefault().Id : 0;
        }

        public Vacancy() { }
    }
}
