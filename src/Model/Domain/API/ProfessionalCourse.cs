﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class ProfessionalCourse
    {
        public virtual Int32 Id { get; set; }

        public String Course { get; set; }
        public String EnumCountry { get; set; }
        public String Institute { get; set; }
        public Int32? Workload { get; set; }
        public DateTime? ConclusionDate { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public ProfessionalCourse(TProfessionalCourse professionalCourse)
        {
            this.Id = professionalCourse.Id;
            this.Course = professionalCourse.Course;
            this.EnumCountry = professionalCourse.EnumCountry.Description();
            this.Institute = professionalCourse.Institute;
            this.Workload = professionalCourse.Workload;
            this.ConclusionDate = professionalCourse.ConclusionDate;
            this.NameCityCountry = professionalCourse.NameCityCountry;
            this.City = professionalCourse.City != null ? professionalCourse.City.CityName : "";
            this.State = professionalCourse.City != null ? professionalCourse.City.State.Sigla : "";

        }
        public ProfessionalCourse() { }
    }
}
