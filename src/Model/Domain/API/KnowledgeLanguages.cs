﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class KnowledgeLanguages
    {
        public List<KnowledgeLanguage> Languages { get; set; }
        public String Error { get; set; }

        public KnowledgeLanguages(List<TKnowledgeLanguage>  languages)
        {
            this.Languages = new List<KnowledgeLanguage>();
            foreach (var lang in languages)
            {
                this.Languages.Add(new KnowledgeLanguage(lang));
            }
        }

        public KnowledgeLanguages() { }
    }
}
