﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class Vacancies
    {
        public List<Vacancy> VacancyList { get; set; }
        public String Error { get; set; }

        public Vacancies(List<TVacancy> vacancy)
        {
            this.VacancyList = new List<Vacancy>();
            foreach (var item in vacancy)
            {
                this.VacancyList.Add(new Vacancy(item));
            }
        }

        public Vacancies() { }
    }
}
