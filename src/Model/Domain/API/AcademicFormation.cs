﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class AcademicFormation
    {
        public  int Id { get; set; }
        public  string TypeFormation { get; set; }
        public  string Country { get; set; }
        public  String Course { get; set; }
        public  String Institute { get; set; }
        public string StatusFormation { get; set; }
        public  DateTime? InitialDate { get; set; }
        public  DateTime? EndDate { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }

        public string Error { get; set; }
        public String Success { get; set; }

        public AcademicFormation(TAcademicFormation academicFormation)
        {
            this.Id = academicFormation.Id;
            this.TypeFormation = academicFormation.EnumTypeFormation.Description();
            this.Course = academicFormation.Course;
            this.EndDate = academicFormation.EndDate;
            this.InitialDate = academicFormation.InitialDate;
            this.Institute = academicFormation.Institute;
            this.StatusFormation = academicFormation.EnumStatusFormation.Description();
            this.Country = academicFormation.EnumCountry.Description();
            this.NameCityCountry = academicFormation.NameCityCountry;
            this.City = academicFormation.City != null ? academicFormation.City.CityName : "";
            this.State = academicFormation.City != null ? academicFormation.City.State.Sigla : "";
        }

        public AcademicFormation(){}
    }
}
