﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class Certificate
    {
        public Int32 Id { get; set; }
        public String Certification { get; set; }
        public Int32 NumberOrganClass { get; set; }
        public String OrganIssuer { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public Certificate(TCertificate certificate)
        {
            this.Id = certificate.Id;
            this.Certification = certificate.Certification;
            this.NumberOrganClass = certificate.NumberOrganClass;
            this.OrganIssuer = certificate.OrganIssuer;
        }

        public Certificate() { }
    }
}
