﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
   public class KnowledgeInformatic
    {

        public Int32 Id { get; set; }
        public String Software { get; set; }
        public String EnumLevel { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public KnowledgeInformatic(TKnowledgeInformatic knowledgeInformatic)
       {
           this.Id = knowledgeInformatic.Id;
           this.Software = knowledgeInformatic.Software;
           this.EnumLevel = knowledgeInformatic.EnumLevel.Description();
       }

        public KnowledgeInformatic() { }
    }
}
