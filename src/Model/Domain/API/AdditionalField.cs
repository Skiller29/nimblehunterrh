﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class AdditionalField
    {
        public Int32 Id { get; set; }
        public String NameField { get; set; }
        public String EnumTypeField { get; set; }
        public Int32 IdVacancy { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public AdditionalField(TAdditionalField field)
        {
            this.Id = field.Id;
            this.NameField = field.NameField;
            this.EnumTypeField = field.EnumTypeField.Description();
            this.IdVacancy = field.Vacancy.Id;
        }

        public AdditionalField() { }
    }
}
