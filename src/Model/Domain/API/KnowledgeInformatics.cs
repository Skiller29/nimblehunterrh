﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
   public class KnowledgeInformatics
    {
        public List<KnowledgeInformatic> Informatics { get; set; }
        public String Error { get; set; }

        public KnowledgeInformatics(List<TKnowledgeInformatic> informatics)
        {
            this.Informatics = new List<KnowledgeInformatic>();
            foreach (var info in informatics)
            {
                this.Informatics.Add(new KnowledgeInformatic(info));
            }
        }

        public KnowledgeInformatics() { }

    }
}
