﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class AdditionalFields
    {
        public List<AdditionalField> Fields { get; set; }
        public String Error { get; set; }

        public AdditionalFields(List<TAdditionalField> fields)
        {
            this.Fields = new List<AdditionalField>();
            foreach (var field in fields)
            {
                this.Fields.Add(new AdditionalField(field));
            }
        }

        public AdditionalFields() { }
    }
}
