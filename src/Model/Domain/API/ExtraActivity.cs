﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class ExtraActivity
    {
        public Int32 Id { get; set; }

        public String  EnumTypeActivity { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String EnumCountry { get; set; }
        public String Description { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public ExtraActivity(TExtraActivity extraActivity)
        {
            this.Id = extraActivity.Id;
            this.EnumTypeActivity = extraActivity.EnumTypeActivity.Description();
            this.InitialDate = extraActivity.InitialDate.Date;
            this.EndDate = extraActivity.EndDate;
            this.EnumCountry = extraActivity.EnumCountry.Description();
            this.Description = extraActivity.Description;
            this.NameCityCountry = extraActivity.NameCityCountry;
            this.City = extraActivity.City != null ? extraActivity.City.CityName : "";
            this.State = extraActivity.City != null ? extraActivity.City.State.Sigla : "";
        }

        public ExtraActivity() { }
    }
}
