﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class User
    {
        public Int32 Id { get; set; }

        public DateTime CreationDate { get; set; }
        public String Cpf { get; set; }
        public String EnumProfile { get; set; }
        public String Token { get; set; }
        public String UrlPhoto { get; set; }
        public String FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public String Nationality { get; set; }
        public String EnumGenre { get; set; }
        public String EnumMaritalStatus { get; set; }
        public Boolean IsDeficient { get; set; }
        public String Address { get; set; }
        public String Cep { get; set; }
        public String Number { get; set; }
        public String Complement { get; set; }
        public String Neighborhood { get; set; }
        public String Email { get; set; }
        public String HomePhone { get; set; }
        public String CellPhone { get; set; }
        public String OptionalPhone { get; set; }
        public bool HasCurriculum { get; set; }
        public int IdCurriculum { get; set; }
        public String State { get; set; }
        public String City { get; set; }
        public String Error { get; set; }

        public User(TUser user)
        {
            this.Id = user.Id;
            this.CreationDate = user.CreationDate;
            this.Cpf = user.Cpf;
            this.EnumProfile = user.EnumProfile.ToString();
            this.Token = user.Token;
            this.UrlPhoto = user.UrlPhoto;
            this.FullName = user.FullName;
            this.Address = user.Address;
            this.BirthDate = user.BirthDate ?? DateTime.MinValue;
            this.Nationality = user.Nationality;
            this.EnumGenre = user.EnumGenre == null ? "" : user.EnumGenre.Description();
            this.EnumMaritalStatus = user.EnumMaritalStatus == null ? "" : user.EnumMaritalStatus.Description();
            this.IsDeficient = user.IsDeficient;
            this.Cep = user.Cep;
            this.Number = user.Number;
            this.Complement = user.Complement;
            this.Neighborhood = user.Neighborhood;
            this.HomePhone = user.HomePhone;
            this.CellPhone = user.CellPhone;
            this.OptionalPhone = user.OptionalPhone;
            this.Email = user.Email;
            this.HasCurriculum = user.TCurriculums.Any();
            this.IdCurriculum = user.TCurriculums.Any() ? user.TCurriculums.FirstOrDefault().Id : 0;
            this.State = user.City != null ? user.City.State.Sigla : "";
            this.City = user.City != null ? user.City.CityName : "";

        }

        public User() { }
    }
}
