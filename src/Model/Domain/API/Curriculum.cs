﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class Curriculum
    {
        public virtual Int32 Id { get; set; }

        public Boolean WorkedCompany { get; set; }
        public String SectorWorked { get; set; }
        public DateTime? AdmitionDateWork { get; set; }
        public DateTime? TerminationDateWork { get; set; }
        public String TerminationReasonWork { get; set; }
        public Boolean HasKinshipWorked { get; set; }
        public String EnumDegreeKinship { get; set; }
        public String NameKinship { get; set; }
        public String SectorKinship { get; set; }
        public String PhoneKinship { get; set; }
        public DateTime LastModification { get; set; }
        public DateTime CreationDate { get; set; }
        public String UrlPhoto { get; set; }
        public String EnumStatusCurriculum { get; set; }
        public String EnumFirstAreaInterest { get; set; }
        public String EnumSecondAreaInterest { get; set; }
        public String EnumThirdAreaInterest { get; set; }
        public String EnumSalaryRequeriments { get; set; }
        public String NameCandidate { get; set; }
        public int IdUser { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public Curriculum(TCurriculum curriculum)
        {
            this.Id = curriculum.Id;
            this.WorkedCompany = curriculum.WorkedCompany;
            this.SectorWorked = curriculum.SectorWorked ?? "";
            this.AdmitionDateWork = curriculum.AdmitionDateWork;
            this.TerminationDateWork = curriculum.TerminationDateWork;
            this.HasKinshipWorked = curriculum.HasKinshipWorked;
            this.EnumDegreeKinship = curriculum.EnumDegreeKinship == null ? "" : curriculum.EnumDegreeKinship.Description();
            this.NameKinship = curriculum.NameKinship ?? "";
            this.SectorKinship = curriculum.SectorKinship ?? "";
            this.PhoneKinship = curriculum.PhoneKinship ?? "";
            this.LastModification = curriculum.LastModification;
            this.CreationDate = curriculum.CreationDate;
            this.EnumStatusCurriculum = curriculum.EnumStatusCurriculum.Description();
            this.EnumFirstAreaInterest = curriculum.EnumFirstAreaInterest.Description();
            this.EnumSecondAreaInterest = curriculum.EnumSecondAreaInterest == null ? "" : curriculum.EnumSecondAreaInterest.Value.Description();
            this.EnumThirdAreaInterest = curriculum.EnumThirdAreaInterest == null ? "" : curriculum.EnumThirdAreaInterest.Value.Description();
            this.EnumSalaryRequeriments = curriculum.EnumSalaryRequeriments == null ? "" : curriculum.EnumSalaryRequeriments.Value.Description();
            this.NameCandidate = curriculum.User.FullName;
            this.IdUser = curriculum.User.Id;
        }

        public Curriculum() { }
    }
}
