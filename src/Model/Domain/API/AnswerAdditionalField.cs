﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class AnswerAdditionalField
    {
        public Int32 Id { get; set; }
        public String Answer { get; set; }
        public String TitleField { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public AnswerAdditionalField(TAnswersAdditionalField answer)
        {
            this.Id = answer.Id;
            this.Answer = answer.Answer;
            this.TitleField = answer.AdditionalField.NameField;
        }

        public AnswerAdditionalField() { }

    }
}
