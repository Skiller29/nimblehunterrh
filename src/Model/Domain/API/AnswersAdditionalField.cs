﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain.API
{
    public class AnswersAdditionalField
    {
        public List<AnswerAdditionalField> Answers { get; set; }
        public String Error { get; set; }

        public AnswersAdditionalField(List<TAnswersAdditionalField> answers)
        {
            this.Answers = new List<AnswerAdditionalField>();
            foreach (var answer in answers)
            {
                this.Answers.Add(new AnswerAdditionalField(answer));
            }
        }

        public AnswersAdditionalField() { }
    }
}
