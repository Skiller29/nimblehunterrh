﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NimbleHunterRH.Web.Helpers;

namespace NimbleHunterRH.Domain.API
{
    public class ProfessionalExperience
    {
        public Int32 Id { get; set; }

        public String Company { get; set; }
        public String Occupation { get; set; }
        public String EnumOccupationArea { get; set; }
        public DateTime AdmissionDate { get; set; }
        public DateTime TerminationDate { get; set; }
        public String ImmediateSuperior { get; set; }
        public String CompanyPhone { get; set; }
        public String EnumCountry { get; set; }
        public Double LastSalary { get; set; }
        public String TerminationReason { get; set; }
        public String ActivitiesPerformed { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public ProfessionalExperience(TProfessionalExperience professionalExperience)
        {
            this.Id = professionalExperience.Id;
            this.Company = professionalExperience.Company;
            this.Occupation = professionalExperience.Occupation;
            this.EnumOccupationArea = professionalExperience.EnumOccupationArea.Description();
            this.AdmissionDate = professionalExperience.AdmissionDate.Date;
            this.TerminationDate = professionalExperience.TerminationDate.Date;
            this.ImmediateSuperior = professionalExperience.ImmediateSuperior;
            this.CompanyPhone = professionalExperience.CompanyPhone;
            this.EnumCountry = professionalExperience.EnumCountry.Description();
            this.LastSalary = professionalExperience.LastSalary.HasValue ? professionalExperience.LastSalary.Value : 0;
            this.TerminationReason = professionalExperience.TerminationReason;
            this.ActivitiesPerformed = professionalExperience.ActivitiesPerformed;
            this.NameCityCountry = professionalExperience.NameCityCountry;
            this.City = professionalExperience.City != null ? professionalExperience.City.CityName : "";
            this.State = professionalExperience.City != null ? professionalExperience.City.State.Sigla : "";
        }

        public ProfessionalExperience(){ }
    }
}
