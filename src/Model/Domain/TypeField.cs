﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NimbleHunterRH.Domain
{
    public enum TypeField
    {
        [Description("Valor inteiro")]
        Inteiro = 0,
        [Description("Valor Decimal")]
        Decimal,
        [Description("Texto")]
        Texto
    }
}
