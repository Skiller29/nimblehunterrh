﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NimbleHunterRH.Domain;
using NimbleHunterRH.Web.Helpers;
using Simple.Web.Mvc;

namespace NimbleHunterRH.Web.Controllers
{
    public partial class EmpresasController : Controller
    {
        // GET: Empresas
        public virtual ActionResult Index()
        {
            foreach (var candidato in TUser.List(x=>x.EnumProfile == Domain.Profile.Candidato))
            {
                new TSelectiveProcess()
                {
                    Curriculum = candidato.TCurriculums.FirstOrDefault(),
                    StepVacancy = TStepsVacancy.Load(7)
                }.Save();
            }
            return View();
        }

        public virtual ActionResult CadastrarEmpresa()
        {
            ViewBag.areasNegocio = EnumHelper.ListAll<LineBusiness>().ToSelectList(x => x, x => x.Description()).SelectValues();
            return View();
        }

        [HttpPost]
        public virtual ActionResult CadastrarEmpresa(TEnterprise model)
        {
            var empresa = new TEnterprise()
            {
                Cep = model.Cep,
                Name = model.Name,
                Cnpj = model.Cnpj,
                Phone = model.Phone,
                EnumLineBusiness = model.EnumLineBusiness,
                CreationDate = DateTime.Now,
                Token = Guid.NewGuid().ToString(),
                IsMultinacional = false
            }.Save();

           return RedirectToAction(MVC.Empresas.Index());
        }

        public virtual ActionResult CadastrarUsuarioEmpresa()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult CadastrarUsuarioEmpresa(TUser user)
        {
            var usuario = new TUser()
            {
                CreationDate = DateTime.Now,
                FullName = user.FullName,
                EnumProfile = Domain.Profile.AdminEmpresa,
                Cpf = user.Cpf,
                Token = Guid.NewGuid().ToString("D").Substring(0, 20),
                Password = TUser.HashPassword(user.PasswordString),
                Enterprise = TEnterprise.Load(1),
                UrlPhoto = "http://www.politize.com.br/wp-content/uploads/2016/08/imagem-sem-foto-de-perfil-do-facebook-1348864936180_956x5001.jpg"
            }.Save();

            return RedirectToAction(MVC.Empresas.CadastrarUsuarioEmpresa());
        }
    }
}