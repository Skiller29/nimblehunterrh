﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NimbleHunterRH.Domain;
using NimbleHunterRH.Web.Helpers;
using NimbleHunterRH.Domain.API;
using Simple;

namespace NimbleHunterRH.Web.Controllers
{
    public partial class APIController : Controller
    {
        #region Usuarios

        public virtual ActionResult Login(string cpf, string senha, string chaveEmpresa)
        {
            var usuario = TUser.Service.Authenticate(cpf, senha);
            if (usuario == null)
                return ContentJsonResult(new User() { Error = "Usuário ou senha inválidos." });

            var empresa = TEnterprise.Find(x => x.Token == chaveEmpresa);
            if (empresa == null)
                return ContentJsonResult(new User() { Error = "Empresa não encontrada." });

            if (usuario.Enterprise.Id != empresa.Id)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado para esta empresa." });

            return ContentJsonResult(new User(usuario));

        }

        public virtual ActionResult CadastrarUsuario(string cpf, string senha, string nome, string chaveEmpresa)
        {
            var empresa = TEnterprise.Find(x => x.Token == chaveEmpresa);
            if (empresa == null)
                return ContentJsonResult(new User() { Error = "Empresa não encontrada." });

            try
            {
                var usuario = new TUser()
                {
                    CreationDate = DateTime.Now,
                    Password = TUser.HashPassword(senha),
                    Cpf = cpf,
                    Enterprise = empresa,
                    Token = Guid.NewGuid().ToString("D").Substring(0, 20),
                    FullName = nome,
                    EnumProfile = Domain.Profile.Candidato,
                    UrlPhoto = "http://www.politize.com.br/wp-content/uploads/2016/08/imagem-sem-foto-de-perfil-do-facebook-1348864936180_956x5001.jpg"
                }.Save();

                return ContentJsonResult(new User(usuario));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new User() { Error = "Por favor, tente novamente." });
            }

        }

        public virtual ActionResult AlterarSenhaUsuario(string chaveUsuario, string senhaOriginal, string novaSenha, string chaveEmpresa)
        {
            var empresa = TEnterprise.Find(x => x.Token == chaveEmpresa);
            if (empresa == null)
                return ContentJsonResult(new User() { Error = "Empresa não encontrada." });

            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado" });

            if (!usuario.Password.SequenceEqual(TUser.HashPassword(senhaOriginal)))
                return ContentJsonResult(new User() { Error = "senha original inválida" });
            try
            {
                usuario.Password = TUser.HashPassword(novaSenha);
                usuario.Update();

                return ContentJsonResult(new User(usuario));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new User() { Error = "Por favor, tente novamente." });
            }
        }

        public virtual ActionResult DetalhesUsuario(string chaveUsuario)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado" });

            return ContentJsonResult(new User(usuario));
        }

        public virtual ActionResult ProcurarUsuario(int idUsuario, string chaveUsuario)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado" });

            var usuarioProcurado = TUser.Load(idUsuario);
            if (usuarioProcurado == null)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado" });

            return ContentJsonResult(new User(usuarioProcurado));
        }

        public virtual ActionResult EditarDadosPessoais(string chaveUsuario, string nomeCidade, string nome, DateTime? dataNascimento, string nacionalidade, string genero, string estadoCivil, bool deficiente, string endereco, string cep, string numero, string complemento, string bairro, string email, string telefoneResidencial, string celular, string telefoneOpcional)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            try
            {
                usuario.City = cidade;
                usuario.Address = endereco;
                usuario.BirthDate = dataNascimento;
                usuario.Cep = cep;
                usuario.Complement = complemento;
                usuario.Email = email;
                usuario.FullName = nome;
                usuario.HomePhone = telefoneResidencial;
                usuario.CellPhone = celular;
                usuario.IsDeficient = deficiente;
                usuario.Nationality = nacionalidade;
                usuario.Neighborhood = bairro;
                usuario.Number = numero;
                usuario.OptionalPhone = telefoneOpcional;
                usuario.EnumGenre = Enum.GetValues(typeof(Genre)).Cast<Genre>().FirstOrDefault(x => x.Description().Equals(genero));
                usuario.EnumMaritalStatus = Enum.GetValues(typeof(MaritalStatus)).Cast<MaritalStatus>().FirstOrDefault(x => x.Description().Equals(estadoCivil));
                usuario.Update();

                return ContentJsonResult(new User(usuario));

            }
            catch (Exception e)
            {
                return ContentJsonResult(new User() { Error = "Por favor, tente novamente." });
            }
        }

        public virtual ActionResult AlterarFotoUsuario(string chaveUsuario)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new User() { Error = "Usuário não encontrado" });

            string uploadArquivo = "";
            string nomeMidia = "";
            var arquivo = Request.Files[0];
            if (arquivo == null)
                return ContentJsonResult(new User() { Error = "Problemas ao enviar o arquivo" });

            try
            {
                nomeMidia = string.Format("{0}{1}{2}", usuario.Id, usuario.FullName, Path.GetExtension(arquivo.FileName));
                uploadArquivo = Server.MapPath("~/Content/img/foto-usuario");
                string caminhoarquivoPerfil = Path.Combine(@uploadArquivo, nomeMidia);
                arquivo.SaveAs(caminhoarquivoPerfil);
                usuario.UrlPhoto = $"http://www.homolog.webroad.com.br/nimblehunterrh/content/img/foto-usuario/{nomeMidia}";
                usuario.Update();

                return ContentJsonResult(new User(usuario));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new User() { Error = "Por favor, tente novamente." });
            }
        }
        #endregion

        #region Curriculos

        public virtual ActionResult DetalhesCurriculo(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculum() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new Curriculum() { Error = "Currículo não encontrado" });

            return ContentJsonResult(new Curriculum(curriculo));
        }

        public virtual ActionResult NovoCurriculo(string chaveUsuario, bool trabalhouEmpresa, string setorTrabalhado, DateTime? dataAdmissaoTrabalhado, DateTime? dataDesligamentoTrabalhado, string motivoDesligamentoTrabalhado, bool parenteEmpresa, string tipoParentesco, string nomeParente, string setorParente, string telefoneParente, string areaInteresseUm, string areaInteresseDois, string areaInteresseTres, string salarioPretendido)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculum() { Error = "Usuário não encontrado" });

            try
            {
                var curriculo = new TCurriculum()
                {
                    User = usuario,
                    AdmitionDateWork = !trabalhouEmpresa ? null : dataAdmissaoTrabalhado,
                    Entreprise = usuario.Enterprise,
                    CreationDate = DateTime.Now,
                    LastModification = DateTime.Now,
                    HasKinshipWorked = parenteEmpresa,
                    NameKinship = !parenteEmpresa ? null : nomeParente,
                    PhoneKinship = !parenteEmpresa ? null : telefoneParente,
                    SectorKinship = !parenteEmpresa ? null : setorParente,
                    SectorWorked = !trabalhouEmpresa ? null : setorTrabalhado,
                    TerminationDateWork = !trabalhouEmpresa ? null : dataDesligamentoTrabalhado,
                    WorkedCompany = trabalhouEmpresa,
                    TerminationReasonWork = !trabalhouEmpresa ? null : motivoDesligamentoTrabalhado,
                    EnumDegreeKinship = !parenteEmpresa ? 0 : (DegreeKinship)System.Enum.Parse(typeof(DegreeKinship), tipoParentesco),
                    EnumStatusCurriculum = StatusCurriculum.Ativo,
                    EnumFirstAreaInterest = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseUm)),
                    EnumSecondAreaInterest = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseDois)),
                    EnumThirdAreaInterest = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseTres)),
                    EnumSalaryRequeriments = Enum.GetValues(typeof(SalaryRequeriments)).Cast<SalaryRequeriments>().FirstOrDefault(x => x.Description().Equals(salarioPretendido))
                }.Save();

                return ContentJsonResult(new Curriculum(curriculo));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Curriculum() { Error = "Por favor, tente novamente." });
            }
        }

        public virtual ActionResult EditarCurriculo(string chaveUsuario, int idCurriculo, bool trabalhouEmpresa, string setorTrabalhado, DateTime? dataAdmissaoTrabalhado, DateTime? dataDesligamentoTrabalhdo, string motivoDesligamentoTrabalhado, bool parenteEmpresa, string tipoParentesco, string nomeParente, string setorParente, string telefoneParente, string areaInteresseUm, string areaInteresseDois, string areaInteresseTres, string salarioPretendido)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculum() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new Curriculum() { Error = "Currículo não encontrado" });

            try
            {
                if (trabalhouEmpresa)
                {
                    curriculo.AdmitionDateWork = dataAdmissaoTrabalhado;
                    curriculo.TerminationDateWork = dataDesligamentoTrabalhdo;
                    curriculo.TerminationReasonWork = motivoDesligamentoTrabalhado;
                    curriculo.SectorWorked = setorTrabalhado;
                }
                else
                {
                    curriculo.AdmitionDateWork = null;
                    curriculo.TerminationDateWork = null;
                    curriculo.TerminationReasonWork = null;
                    curriculo.SectorWorked = null;
                }

                if (parenteEmpresa)
                {
                    curriculo.NameKinship = nomeParente;
                    curriculo.PhoneKinship = telefoneParente;
                    curriculo.SectorKinship = setorParente;
                    curriculo.EnumDegreeKinship = Enum.GetValues(typeof(DegreeKinship)).Cast<DegreeKinship>().FirstOrDefault(x => x.Description().Equals(tipoParentesco));

                }
                else
                {
                    curriculo.NameKinship = null;
                    curriculo.PhoneKinship = null;
                    curriculo.SectorKinship = null;
                    curriculo.EnumDegreeKinship = 0;
                }
                curriculo.LastModification = DateTime.Now;
                curriculo.HasKinshipWorked = parenteEmpresa;
                curriculo.WorkedCompany = trabalhouEmpresa;
                curriculo.EnumFirstAreaInterest = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseUm));
                curriculo.EnumSecondAreaInterest = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseDois));
                curriculo.EnumThirdAreaInterest = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseTres));
                curriculo.EnumSalaryRequeriments = Enum.GetValues(typeof(SalaryRequeriments)).Cast<SalaryRequeriments>().FirstOrDefault(x => x.Description().Equals(salarioPretendido));
                curriculo.Update();

                return ContentJsonResult(new Curriculum(curriculo));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Curriculum() { Error = "Por favor, tente novamente." });
            }
        }
        #endregion

        #region Buscar Curriculos

        public virtual ActionResult BuscarPorCertificacao(string chaveUsuario, string certificacao, string orgaoEmissor, int numeroOrgao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var curriculos = TCertificate.Service.SearchCurriculums(certificacao, orgaoEmissor, numeroOrgao);
            return ContentJsonResult(new Curriculums(curriculos.Where(x=>x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorIdioma(string chaveUsuario, string idioma, string conversacao, string leitura, string escrita)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var enumIdioma = Enum.GetValues(typeof (Language)).Cast<Language>().FirstOrDefault(x => x.Description().Equals(idioma));
            var enumConversacao = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(conversacao));
            var enumEscrita = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(escrita));
            var enumLeitura = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(leitura));

            var curriculos = TKnowledgeLanguage.Service.SearchCurriculums(enumIdioma, enumConversacao, enumEscrita, enumLeitura);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorInformatica(string chaveUsuario, string software, string nivel)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var enumNivel = Enum.GetValues(typeof(Level)).Cast<Level>().FirstOrDefault(x => x.Description().Equals(nivel));
            var curriculos = TKnowledgeInformatic.Service.SearchCurriculums(software, enumNivel);

            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorAtividadeExtra(string chaveUsuario, string tipoAtividade, string pais, string nomeCidadePais, string nomeCidade)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            var enumTipoAtividade = Enum.GetValues(typeof(TypeActivity)).Cast<TypeActivity>().FirstOrDefault(x => x.Description().Equals(tipoAtividade));
            var enumPais = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));

            var curriculos = TExtraActivity.Service.SearchCurriculums(enumTipoAtividade, enumPais, cidade, nomeCidadePais);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorFormacaoAcademica(string chaveUsuario, string status, string tipoFormacao, string curso, string instituicao, string pais, string nomeCidadePais, string nomeCidade)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            var enumTipoFormacao = Enum.GetValues(typeof(TypeFormation)).Cast<TypeFormation>().FirstOrDefault(x => x.Description().Equals(tipoFormacao));
            var enumStatus = Enum.GetValues(typeof(StatusFormation)).Cast<StatusFormation>().FirstOrDefault(x => x.Description().Equals(status));
            var enumPais = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));

            var curriculos = TAcademicFormation.Service.SearchCurriculums(enumStatus, enumTipoFormacao, curso, instituicao, enumPais, cidade, nomeCidadePais);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorDadosPessoais(string chaveUsuario, string nome, string genero, string estadoCivil, string nomeCidade)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            var enumGenero = Enum.GetValues(typeof(Genre)).Cast<Genre>().FirstOrDefault(x => x.Description().Equals(genero));
            var enumEstadoCivil = Enum.GetValues(typeof(MaritalStatus)).Cast<MaritalStatus>().FirstOrDefault(x => x.Description().Equals(estadoCivil));

            var curriculos = TUser.Service.SearchCurriculums(nome, enumGenero, enumEstadoCivil, cidade);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));

        }

        public virtual ActionResult BuscarPorObjetivos(string chaveUsuario, string areaInteresseUm, string areaInteresseDois, string areaInteresseTres, string salarioPretendido)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var enumAreaInteresseUm = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseUm));
            var enumAreaInteresseDois = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseDois));
            var enumAreaInteresseTres = Enum.GetValues(typeof(AreaInterest)).Cast<AreaInterest>().FirstOrDefault(x => x.Description().Equals(areaInteresseTres));
            var enumSalarioPretendido = Enum.GetValues(typeof(SalaryRequeriments)).Cast<SalaryRequeriments>().FirstOrDefault(x => x.Description().Equals(salarioPretendido));

            var curriculos = TCurriculum.Service.SearchCurrilums(enumAreaInteresseUm, enumAreaInteresseDois, enumAreaInteresseTres, enumSalarioPretendido);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorCursoProfissionalizante(string chaveUsuario, string curso, string instituicao, string pais, string nomeCidadePais, string nomeCidade)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            var enumPais = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));

            var curriculos = TProfessionalCourse.Service.SearchCurriculums(curso, instituicao, enumPais, cidade, nomeCidadePais);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }

        public virtual ActionResult BuscarPorExperienciaProfissional(string chaveUsuario, string companhia, string ocupacao, string areaOcupacao, string pais, string nomeCidadePais, string nomeCidade)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Curriculums() { Error = "Usuário não encontrado" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            var enumAreaOcupacao = Enum.GetValues(typeof(OccupationArea)).Cast<OccupationArea>().FirstOrDefault(x => x.Description().Equals(areaOcupacao));
            var enumPais = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));

            var curriculos = TProfessionalExperience.Service.SearchCurriculums(enumAreaOcupacao, ocupacao, companhia, enumPais, cidade, nomeCidadePais);
            return ContentJsonResult(new Curriculums(curriculos.Where(x => x.User.Enterprise.Id == usuario.Enterprise.Id).ToList()));
        }
        #endregion

        #region Processos Seletivos

        public virtual ActionResult DetalhesProcessoSeletivoCandidato(string chaveUsuario, int idProcesso)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Usuário não encontrado" });

            var processo = TSelectiveProcess.Load(idProcesso);
            if (processo == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Processo não encontrado" });

            return ContentJsonResult(new SelectiveProcess(processo));
        }

        public virtual ActionResult ListarProcessosSeletivosCandidato(string chaveUsuario)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcesses() { Error = "Usuário não encontrado" });

            var processos = TSelectiveProcess.List(x => x.Curriculum.User.Id == usuario.Id).Distinct().ToList();

            return ContentJsonResult(new SelectiveProcesses(processos));
        }

        public virtual ActionResult ListarProcessosSeletivosEtapa(string chaveUsuario, int idEtapa)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcesses() { Error = "Usuário não encontrado" });

            var etapa = TStepsVacancy.Load(idEtapa);
            if (etapa == null)
                return ContentJsonResult(new SelectiveProcesses() { Error = "Usuário não encontrado" });

            var processos = TSelectiveProcess.List(x => x.StepVacancy.Id == etapa.Id).ToList();
            return ContentJsonResult(new SelectiveProcesses(processos));
        }

        public virtual ActionResult AprovarCandidatosProximaEtapa(string chaveUsuario, int idEtapa, List<int> idCandidatosAprovados, int? idProximaEtapa, bool enviarEmail, string mensagemAprovados, string mensagemReprovados)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcesses() { Error = "Usuário não encontrado" });

            var etapa = TStepsVacancy.Load(idEtapa);
            if (etapa == null)
                return ContentJsonResult(new SelectiveProcesses() { Error = "etapa não encontrada" });

            TStepsVacancy proximaEtapa = new TStepsVacancy();

            if (idProximaEtapa.HasValue)
            {
                proximaEtapa = TStepsVacancy.Load(idProximaEtapa);
                if (proximaEtapa == null)
                    return ContentJsonResult(new SelectiveProcesses() {Error = "Próxima etapa não encontrada"});

                List<TSelectiveProcess> processos = new List<TSelectiveProcess>();
                foreach (var id in idCandidatosAprovados)
                {
                    var candidato = TSelectiveProcess.Load(id);
                    candidato.IsApproved = true;
                    candidato.Update();

                    var processo = new TSelectiveProcess()
                    {
                        StepVacancy = proximaEtapa,
                        Curriculum = candidato.Curriculum,
                    }.Save();
                    processos.Add(processo);

                    if (enviarEmail && !string.IsNullOrWhiteSpace(candidato.Curriculum.User.Email))
                        new MailService().Send("bruno.skiller@gmail.com", proximaEtapa.Vacancy.Enterprise.Name, candidato.Curriculum.User.Email,candidato.Curriculum.User.FullName, "Parabéns - Candidato aprovado - " + proximaEtapa.Vacancy.Enterprise.Name, mensagemAprovados);
                }
                if (enviarEmail)
                {
                    var candidatosNaoAprovados =
                        TSelectiveProcess.List(x => !x.IsApproved && x.StepVacancy.Id == etapa.Id).ToList();
                    foreach (var candidato in candidatosNaoAprovados)
                    {
                        if (!string.IsNullOrWhiteSpace(candidato.Curriculum.User.Email))
                            new MailService().Send("bruno.skiller@gmail.com", proximaEtapa.Vacancy.Enterprise.Name, candidato.Curriculum.User.Email, candidato.Curriculum.User.FullName, "Obrigado pela participação - " + proximaEtapa.Vacancy.Enterprise.Name, mensagemReprovados);
                    }
                }
                etapa.EnumStatus = Status.Concluido;
                etapa.Update();
                proximaEtapa.EnumStatus = Status.EmAndamento;
                proximaEtapa.Update();

                return ContentJsonResult(new SelectiveProcesses(processos));
            }
            else
            {
                foreach (var id in idCandidatosAprovados)
                {
                    var candidato = TSelectiveProcess.Load(id);
                    candidato.IsApproved = true;
                    candidato.Update();

                    if (enviarEmail && !string.IsNullOrWhiteSpace(candidato.Curriculum.User.Email))
                        new MailService().Send("bruno.v.andre@gmail.com", "Bruno TCC", candidato.Curriculum.User.Email,
                            "Brunao", "Teste email TCC",
                            "<p>Teste mensagem email.</p><br/><br/><p>Enviado do sistema NimbleHunter</p>");
                }
                if (enviarEmail)
                {
                    var candidatosNaoAprovados =
                        TSelectiveProcess.List(x => !x.IsApproved && x.StepVacancy.Id == etapa.Id).ToList();
                    foreach (var candidato in candidatosNaoAprovados)
                    {
                        if (!string.IsNullOrWhiteSpace(candidato.Curriculum.User.Email))
                            new MailService().Send("bruno.v.andre@gmail.com", "Bruno TCC",
                                candidato.Curriculum.User.Email, "Brunao", "Teste email TCC",
                                "<p>Teste mensagem email.</p><br/><br/><p>Enviado do sistema NimbleHunter</p>");
                    }
                }
                etapa.EnumStatus = Status.Concluido;
                etapa.Update();
                if (etapa.Vacancy.TStepsVacancies.All(x => x.EnumStatus == Status.Concluido))
                {
                    var vaga = etapa.Vacancy;
                    vaga.EnumStatus = Status.Concluido;
                    vaga.Update();
                }
            }
            return ContentJsonResult(new SelectiveProcesses(etapa.TSelectiveProcesses.ToList()));
        }

        public virtual ActionResult ExcluirProcessoSeletivoCandidato(string chaveUsuario, int idProcesso)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Usuário não encontrado" });

            var processo = TSelectiveProcess.Load(idProcesso);
            if (processo == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Processo não encontrado" });

            processo.Delete();
            return ContentJsonResult(new SelectiveProcess() { Success = "Processo excluído com sucesso" });
        }

        public virtual ActionResult InserirCandidatoEtapa(string chaveUsuario, List<int> idCurriculum, int idEtapa)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcesses() { Error = "Usuário não encontrado" });

            var etapa = TStepsVacancy.Load(idEtapa);
            if (etapa == null)
                return ContentJsonResult(new SelectiveProcesses() {Error = "Etapa não encontrada"});

            List<TSelectiveProcess> processos = new List<TSelectiveProcess>();
            foreach (var id in idCurriculum)
            {
                var curriculo = TCurriculum.Load(id);
                if (curriculo == null)
                    return ContentJsonResult(new SelectiveProcesses() { Error = "Curriculo não encontrado" });

                if(etapa.TSelectiveProcesses.Any(x=>x.Curriculum.Id == curriculo.Id))
                    return ContentJsonResult(new SelectiveProcesses() { Error = "O candidato " + curriculo.User.FullName + " já faz parte desta etapa." });

                var processo = new TSelectiveProcess()
                {
                    StepVacancy = etapa,
                    Curriculum = curriculo,
                }.Save();

                processos.Add(processo);
            }

            return ContentJsonResult(new SelectiveProcesses(processos));
        }

        public virtual ActionResult EditarObservacaoProcesso(string chaveUsuario, int idProcesso, string observacao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Usuário não encontrado" });

            var processo = TSelectiveProcess.Load(idProcesso);
            if (processo == null)
                return ContentJsonResult(new SelectiveProcess() {Error = "Processo não encontrado"});

            processo.Observation = observacao;
            processo.Update();

            return ContentJsonResult(new Vacancy(processo.StepVacancy.Vacancy));
        }
        #endregion

        #region Empresas
        public virtual ActionResult EditarEmpresa(string chaveEmpresa, int idCidade, string nome, string cnpj, string cep, string bairro, string siteEmpresa, bool multinacional, string telefone, string linhaNegocio)
        {
            var empresa = TEnterprise.Find(x => x.Token == chaveEmpresa);
            if (empresa == null)
                return ContentJsonResult(new Enterprise() { Error = "Empresa não encontrada." });

            var cidade = TCity.Load(idCidade);
            if (cidade == null)
                return ContentJsonResult(new Enterprise() { Error = "Cidade não encontrada" });

            try
            {
                empresa.Cnpj = cnpj;
                empresa.Cep = cep;
                empresa.Phone = telefone;
                empresa.Neighbohood = bairro;
                empresa.CompanyWebsite = siteEmpresa;
                empresa.IsMultinacional = multinacional;
                empresa.Name = nome;
                empresa.EnumLineBusiness = (LineBusiness)System.Enum.Parse(typeof(LineBusiness), linhaNegocio);
                empresa.City = cidade;
                empresa.Update();

                return ContentJsonResult(new Enterprise(empresa));

            }
            catch (Exception e)
            {
                return ContentJsonResult(new Enterprise() { Error = "Por favor, tente novamente." });
            }



        }
        #endregion

        #region Vagas
        public virtual ActionResult DetalhesVagas(string chaveUsuario, int idVaga)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Vacancy() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new Vacancy() { Error = "Vaga não encontrada" });

            return ContentJsonResult(new Vacancy(vaga));
        }

        public virtual ActionResult NovaVaga(string chaveUsuario, string chaveEmpresa, string nomeCidade, string ocupacao, bool vagaDeficiente, bool visivelCandidato, string descricao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Vacancy() { Error = "Usuário não encontrado" });

            var empresa = TEnterprise.Find(x=>x.Token.Equals(chaveEmpresa));
            if (empresa == null)
                return ContentJsonResult(new Vacancy() { Error = "Empresa não encontrada" });

            var cidade = TCity.Find(x=>x.CityName.Equals(nomeCidade));
            if (cidade == null)
                return ContentJsonResult(new Vacancy() { Error = "Cidade não encontrada" });

            try
            {
                var vaga = new TVacancy()
                {
                    Description = descricao,
                    IsVacancyDeficient = vagaDeficiente,
                    IsVisibleCandidate = visivelCandidato,
                    Ocupation = ocupacao,
                    Enterprise = empresa,
                    City = cidade,
                    EnumStatus = Status.EmAndamento
                }.Save();

                return ContentJsonResult(new Vacancy(vaga));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Vacancy() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarVaga(string chaveUsuario, int idVaga, string nomeCidade, string ocupacao, bool vagaDeficiente, bool visivelCandidato, string descricao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Vacancy() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new Vacancy() { Error = "Vaga não encontrada" });

            var cidade = TCity.Find(x => x.CityName.Equals(nomeCidade));
            if (cidade == null)
                return ContentJsonResult(new Vacancy() { Error = "Cidade não encontrada" });

            try
            {
                vaga.Description = descricao; ;
                vaga.IsVacancyDeficient = vagaDeficiente;
                vaga.IsVisibleCandidate = visivelCandidato;
                vaga.Ocupation = ocupacao;
                vaga.City = cidade;
                vaga.Update();

                return ContentJsonResult(new Vacancy(vaga));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Vacancy() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ExcluirVaga(string chaveUsuario, int idVaga)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Vacancy() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new Vacancy() { Error = "Vaga não encontrada" });

            try
            {

                if (vaga.TStepsVacancies.Any())
                    vaga.TStepsVacancies.ForEach(x => x.Delete());

                vaga.Delete();
                return ContentJsonResult(new Vacancy() { Success = "Vaga removida com sucesso" });
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Vacancy() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarVagasEmpresa(string chaveUsuario, string chaveEmpresa)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Vacancies() { Error = "Usuário não encontrado" });

            var empresa = TEnterprise.Find(x => x.Token == chaveEmpresa);
            if (empresa == null)
                return ContentJsonResult(new Vacancies() { Error = "Empresa não encontrada." });

            var vagas = TVacancy.List(x => x.Enterprise.Id == empresa.Id).ToList();

            return ContentJsonResult(new Vacancies(vagas));
        }

        public virtual ActionResult ListarVagasDisponiveisEmpresa(string chaveUsuario, string chaveEmpresa)
        {
           var empresa = TEnterprise.Find(x => x.Token == chaveEmpresa);
            if (empresa == null)
                return ContentJsonResult(new Vacancies() { Error = "Empresa não encontrada." });

            List<TVacancy> vagas = new List<TVacancy>();
            if (!string.IsNullOrWhiteSpace(chaveUsuario))
            {
                var usuario = TUser.Find(x => x.Token == chaveUsuario);
                if (usuario == null)
                    return ContentJsonResult(new Vacancies() { Error = "Usuário não encontrado" });

                var etapas = TVacancy.List(x => x.Enterprise.Id == empresa.Id && x.IsVisibleCandidate && x.EnumStatus == Status.EmAndamento && x.TStepsVacancies.Count(y => y.EnumStatus == Status.EmAndamento && y.EnumStep == Step.Anuncio && y.InitialDate <= DateTime.Now && y.EndTime >= DateTime.Now) > 0).Select(z => z.TStepsVacancies.FirstOrDefault(x => x.EnumStep == Step.Anuncio)).ToList();
                vagas = etapas.Where(x => x.TSelectiveProcesses.Count(y => y.Curriculum.Id == usuario.TCurriculums.FirstOrDefault().Id) == 0).Select(x => x.Vacancy).ToList();
            }
            else
            {
                vagas = TVacancy.List(x =>x.Enterprise.Id == empresa.Id && x.IsVisibleCandidate && x.EnumStatus == Status.EmAndamento && x.TStepsVacancies.Count(y =>y.EnumStatus == Status.EmAndamento && y.EnumStep == Step.Anuncio && y.InitialDate <= DateTime.Now && y.EndTime >= DateTime.Now) > 0).ToList();
            }
            
            return ContentJsonResult(new Vacancies(vagas));
        }

        public virtual ActionResult CandidatarVaga(string chaveUsuario, int idVaga, List<string> respostas)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new SelectiveProcess() { Error = "Vaga não encontrada" });

            var etapa = vaga.TStepsVacancies.FirstOrDefault(x => x.EnumStep == Step.Anuncio);

            var register = TSelectiveProcess.List(x => x.Curriculum.User.Id == usuario.Id && x.StepVacancy.Id == etapa.Id);
            if(register != null && register.Any())
                return ContentJsonResult(new SelectiveProcess() { Error = "Você já está cadastrado nesta etapa da vaga." });

            try
            {
                if (vaga.TAdditionalFields.Any())
                {
                    if(vaga.TAdditionalFields.Count != respostas.Count)
                        return ContentJsonResult(new SelectiveProcess() { Error = "A quantidade de respostas não é igual a quantidade perguntas adicionais." });

                    int i = 0;
                    foreach (var pergunta in vaga.TAdditionalFields)
                    {
                        var r = new TAnswersAdditionalField()
                        {
                            AdditionalField = pergunta,
                            Answer = respostas[i],
                            Curriculum = usuario.TCurriculums.FirstOrDefault()
                        }.Save();
                        i++;
                    }                   
                }
                var processo = new TSelectiveProcess()
                {
                    Curriculum = usuario.TCurriculums.FirstOrDefault(),
                    StepVacancy = vaga.TStepsVacancies.FirstOrDefault(x=>x.EnumStep == Step.Anuncio)
                }.Save();

                return ContentJsonResult(new SelectiveProcess(processo));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new SelectiveProcess() { Error = "Por favor, tente novamente" });
            }         
        }

        public virtual ActionResult VagasDisponiveisInsercaoCandidato(string chaveUsuario)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Vacancies() { Error = "Usuário não encontrado" });

            var vagas = TVacancy.List(x => x.Enterprise.Id == usuario.Enterprise.Id && x.EnumStatus != Status.Concluido).ToList();
            return ContentJsonResult(new Vacancies(vagas));
        }

        //ETAPAS
        public virtual ActionResult DetalhesEtapaVaga(string chaveUsuario, int idEtapa)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Usuário não encontrado" });

            var etapa = TStepsVacancy.Load(idEtapa);
            if(etapa == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Etapa não encontrada" });

            return ContentJsonResult(new Stepvacancy(etapa));
        }

        public virtual ActionResult NovaEtapavaga(string chaveUsuario, int idVaga, string tipoEtapa, DateTime dataInicial, DateTime dataFinal)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Vaga não encontrada" });

          try
            {
                var etapa = new TStepsVacancy()
                {
                    Vacancy = vaga,
                    EndTime = dataFinal,
                    InitialDate = dataInicial,
                    EnumStatus = vaga.TStepsVacancies.Any() ? Status.Pendente : Status.EmAndamento,
                    EnumStep = Enum.GetValues(typeof(Step)).Cast<Step>().FirstOrDefault(x => x.Description().Equals(tipoEtapa))
                }.Save();

                return ContentJsonResult(new Stepvacancy(etapa));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Stepvacancy() { Error = "Por favor tente novamente" });
            }

           
        }

        public virtual ActionResult EditarEtapaVaga(string chaveUsuario, int idEtapa, string tipoEtapa, DateTime dataInicial, DateTime dataFinal)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Usuário não encontrado" });

            var etapa = TStepsVacancy.Load(idEtapa);
            if(etapa == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Etapa não encontrada" });

            try
            {
                etapa.EndTime = dataFinal;
                etapa.InitialDate = dataInicial;
                etapa.EnumStep = Enum.GetValues(typeof(Step)).Cast<Step>().FirstOrDefault(x => x.Description().Equals(tipoEtapa));
                etapa.Update();

                return ContentJsonResult(new Stepvacancy(etapa));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Stepvacancy() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ExcluirEtapaVaga(string chaveUsuario, int idEtapa)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Usuário não encontrado" });

            var etapa = TStepsVacancy.Load(idEtapa);
            if (etapa == null)
                return ContentJsonResult(new Stepvacancy() { Error = "Etapa não encontrada" });

            if(etapa.TSelectiveProcesses.Any())
                return ContentJsonResult(new Stepvacancy() { Error = "Esta etapa está vinculada a um processo seletivo." });

            etapa.Delete();

            return ContentJsonResult(new Stepvacancy() {Success = "Etapa excluída com sucesso."});
        }

        public virtual ActionResult ListarEtapasVaga(string chaveUsuario, int idVaga)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new StepsVacancy() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new StepsVacancy() { Error = "Vaga não encontrada" });

            return ContentJsonResult(new StepsVacancy(vaga.TStepsVacancies.ToList()));
        }

        public virtual ActionResult EtapasDisponiveisInsercaoCandidato(string chaveUsuario, int idVaga)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new StepsVacancy() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new StepsVacancy() { Error = "Vaga não encontrada" });

            var etapas = vaga.TStepsVacancies.Where(x => x.EnumStatus != Status.Concluido).ToList();
            return ContentJsonResult(new StepsVacancy(etapas));
        }

        #endregion

        #region Perguntas Adicionais

        public virtual ActionResult DetalhesCampoAdicional(string chaveUsuario, int idCampo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AdditionalField() { Error = "Usuário não encontrado" });

            var campo = TAdditionalField.Load(idCampo);
            if (campo == null)
                return ContentJsonResult(new AdditionalField() { Error = "Pergunta não encontrada" });

            return ContentJsonResult(new AdditionalField(campo));
        }

        public virtual ActionResult NovoCampoAdicional(string chaveUsuario, int idVaga, string tituloCampo, string tipoCampo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AdditionalField() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new AdditionalField() {Error = "Vaga não encontrada"});

            try
            {
                var campo = new TAdditionalField()
                {
                    Vacancy = vaga,
                    NameField = tituloCampo,
                    EnumTypeField = Enum.GetValues(typeof(TypeField)).Cast<TypeField>().FirstOrDefault(x => x.Description().Equals(tipoCampo))
                }.Save();

                return ContentJsonResult(new AdditionalField(campo));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new AdditionalField() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarCampoAdicional(string chaveUsuario, int idCampo, string tituloCampo, string tipoCampo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AdditionalField() { Error = "Usuário não encontrado" });

            var campo = TAdditionalField.Load(idCampo);
            if (campo == null)
                return ContentJsonResult(new AdditionalField() { Error = "Pergunta não encontrada" });

            try
            {
                campo.NameField = tituloCampo;
                campo.EnumTypeField = Enum.GetValues(typeof (TypeField)).Cast<TypeField>().FirstOrDefault(x => x.Description().Equals(tipoCampo));
                campo.Update();

                return ContentJsonResult(new AdditionalField(campo));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new AdditionalField() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ExcluirCampoAdicional(string chaveUsuario, int idCampo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AdditionalField() { Error = "Usuário não encontrado" });

            var campo = TAdditionalField.Load(idCampo);
            if (campo == null)
                return ContentJsonResult(new AdditionalField() { Error = "Pergunta não encontrada" });

            if(campo.TAnswersAdditionalFields.Any())
                return ContentJsonResult(new AdditionalField() { Error = "Não é possível excluir o campo pois ja foi respondido." });

            campo.Delete();
            return ContentJsonResult(new AdditionalField() {Success = "Campo excluído com sucesso."});
        }

        public virtual ActionResult ListarCamposAdicionaisVaga(string chaveUsuario, int idVaga)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AdditionalFields() { Error = "Usuário não encontrado" });

            var vaga = TVacancy.Load(idVaga);
            if (vaga == null)
                return ContentJsonResult(new AdditionalFields() { Error = "Vaga não encontrada" });

            return ContentJsonResult(new AdditionalFields(TAdditionalField.List(x => x.Vacancy.Id == vaga.Id).ToList()));
        }

        public virtual ActionResult ListarCamposAdicionaisUsuario(string chaveUsuario)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AdditionalField() { Error = "Usuário não encontrado" });

            var campos = TAdditionalField.List(x => x.Vacancy.Enterprise.Id == usuario.Enterprise.Id).ToList();
            return ContentJsonResult(new AdditionalFields(campos));
        }
        #endregion

        #region Certificado

        public virtual ActionResult DetalhesCertificado(string chaveUsuario, int idCertificado)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Certificate() { Error = "Usuário não encontrado" });

            var certificado = TCertificate.Load(idCertificado);
            if (certificado == null)
                return ContentJsonResult(new Certificate() { Error = "Certificado não encontrado" });

            return ContentJsonResult(new Certificate(certificado));
        }

        public virtual ActionResult NovoCertificado(string chaveUsuario, int idCurriculo, string certificacao, int numeroOrgaoClasse, string orgaoEmissor)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Certificate() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new Certificate() { Error = "Currículo não encontrado" });

            try
            {
                var certificado = new TCertificate()
                {
                    Curriculum = curriculo,
                    Certification = certificacao,
                    NumberOrganClass = numeroOrgaoClasse,
                    OrganIssuer = orgaoEmissor
                }.Save();

                return ContentJsonResult(new Certificate(certificado));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Certificate() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarCertificado(string chaveUsuario, int idCertificado, string certificacao, int numeroOrgaoClasse, string orgaoEmissor)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Certificate() { Error = "Usuário não encontrado" });

            var certificado = TCertificate.Load(idCertificado);
            if (certificado == null)
                return ContentJsonResult(new Certificate() { Error = "Certificado não encontrado" });

            try
            {
                certificado.Certification = certificacao;
                certificado.NumberOrganClass = numeroOrgaoClasse;
                certificado.OrganIssuer = orgaoEmissor;
                certificado.Update();

                return ContentJsonResult(new Certificate(certificado));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Certificate() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ExcluirCertificado(string chaveUsuario, int idCertificado)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new Certificate() { Error = "Usuário não encontrado" });

            var certificado = TCertificate.Load(idCertificado);
            if (certificado == null)
                return ContentJsonResult(new Certificate() { Error = "Certificado não encontrado" });

            try
            {
                certificado.Delete();
                return ContentJsonResult(new Certificate() { Success = "Certificado removido com sucesso" });
            }
            catch (Exception e)
            {
                return ContentJsonResult(new Certificate() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarCertificacoes(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeLanguages() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new KnowledgeLanguages() { Error = "Currículo não encontrado" });

            var certificacoes = TCertificate.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new Certificates(certificacoes));
        }
        #endregion

        #region Idiomas

        public virtual ActionResult DetalhesIdioma(string chaveUsuario, int idIdioma)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Usuário não encontrado" });

            var idioma = TKnowledgeLanguage.Load(idIdioma);
            if (idioma == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Idioma não encontrado" });

            return ContentJsonResult(new KnowledgeLanguage(idioma));
        }

        public virtual ActionResult NovoIdioma(string chaveUsuario, int idCurriculo, string idioma, string nivelConversacao, string nivelLeitura, string nivelEscrita)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Currículo não encontrado" });

            try
            {
                var model = new TKnowledgeLanguage()
                {
                    EnumConversation = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(nivelConversacao)),
                    EnumReading = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(nivelLeitura)),
                    EnumWriting = (Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(nivelEscrita))),
                    EnumLanguage = (Enum.GetValues(typeof(Language)).Cast<Language>().FirstOrDefault(x => x.Description().Equals(idioma))),
                    Curriculum = curriculo
                }.Save();

                return ContentJsonResult(new KnowledgeLanguage(model));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Por favor tente novamente" });
            }

        }

        public virtual ActionResult EditarIdioma(string chaveUsuario, int idIdioma, string idioma, string nivelConversacao, string nivelLeitura, string nivelEscrita)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Usuário não encontrado" });

            var model = TKnowledgeLanguage.Load(idIdioma);
            if (model == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Idioma não encontrado" });

            try
            {
                model.EnumConversation = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(nivelConversacao));
                model.EnumLanguage = (Enum.GetValues(typeof(Language)).Cast<Language>().FirstOrDefault(x => x.Description().Equals(idioma)));
                model.EnumReading = Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(nivelLeitura));
                model.EnumWriting = (Enum.GetValues(typeof(LevelLanguage)).Cast<LevelLanguage>().FirstOrDefault(x => x.Description().Equals(nivelEscrita)));
                model.Update();

                return ContentJsonResult(new KnowledgeLanguage(model));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ExcluirIdioma(string chaveUsuario, int idIdioma)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Usuário não encontrado" });

            var idioma = TKnowledgeLanguage.Load(idIdioma);
            if (idioma == null)
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Idioma não encontrado" });

            try
            {
                idioma.Delete();
                return ContentJsonResult(new KnowledgeLanguage() { Success = "Idioma excluído com sucesso" });
            }
            catch (Exception e)
            {
                return ContentJsonResult(new KnowledgeLanguage() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarConhecimentosIdioma(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeLanguages() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new KnowledgeLanguages() { Error = "Currículo não encontrado" });

            var idiomas = TKnowledgeLanguage.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new KnowledgeLanguages(idiomas));
        }
        #endregion

        #region Informatica

        public virtual ActionResult DetalhesInformatica(string chaveUsuario, int idInformatica)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Usuário não encontrado" });

            var informatica = TKnowledgeInformatic.Load(idInformatica);
            if (informatica == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Informática não encontrado" });

            return ContentJsonResult(new KnowledgeInformatic(informatica));
        }

        public virtual ActionResult NovoInformatica(string chaveUsuario, int idCurriculo, string software, string nivel)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Currículo não encontrado" });

            try
            {
                var informatica = new TKnowledgeInformatic()
                {
                    Curriculum = curriculo,
                    EnumLevel = Enum.GetValues(typeof(Level)).Cast<Level>().FirstOrDefault(x => x.Description().Equals(nivel)),
                    Software = software
                }.Save();

                return ContentJsonResult(new KnowledgeInformatic(informatica));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarInformatica(string chaveUsuario, int idInformatica, string software, string nivel)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Usuário não encontrado" });

            var informatica = TKnowledgeInformatic.Load(idInformatica);
            if (informatica == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Informática não encontrado" });

            try
            {
                informatica.EnumLevel = Enum.GetValues(typeof(Level)).Cast<Level>().FirstOrDefault(x => x.Description().Equals(nivel));
                informatica.Software = software;
                informatica.Update();

                return ContentJsonResult(new KnowledgeInformatic(informatica));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ExcluirInformatica(string chaveUsuario, int idInformatica)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Usuário não encontrado" });

            var informatica = TKnowledgeInformatic.Load(idInformatica);
            if (informatica == null)
                return ContentJsonResult(new KnowledgeInformatic() { Error = "Informática não encontrado" });

            informatica.Delete();
            return ContentJsonResult(new KnowledgeInformatic() { Success = "Informática excluída com sucesso" });
        }

        public virtual ActionResult ListarConhecimentosInformatica(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new KnowledgeInformatics() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new KnowledgeInformatics() { Error = "Currículo não encontrado" });

            var informatica = TKnowledgeInformatic.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new KnowledgeInformatics(informatica));
        }
        #endregion

        #region Atividades Extras

        public virtual ActionResult DetalhesAtividadeExtra(string chaveUsuario, int idAtividadeExtra)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Usuário não encontrado" });

            var atividadeExtra = TExtraActivity.Load(idAtividadeExtra);
            if (atividadeExtra == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Atividade extra não encontrado" });

            return ContentJsonResult(new ExtraActivity(atividadeExtra));
        }

        public virtual ActionResult NovoAtividadeExtra(string chaveUsuario, int idCurriculo, string cidadePais, string nomeCidade, string tipoAtividade, DateTime dataInicial, DateTime? dataFinal, string pais, string descricao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Currículo não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new ExtraActivity() { Error = "Local não encontrado" });

            try
            {
                var atividadeExtra = new TExtraActivity()
                {
                    Description = descricao,
                    EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais)),
                    EndDate = dataFinal == DateTime.MinValue ? null : dataFinal,
                    InitialDate = dataInicial,
                    Curriculum = curriculo,
                    EnumTypeActivity = Enum.GetValues(typeof(TypeActivity)).Cast<TypeActivity>().FirstOrDefault(x => x.Description().Equals(tipoAtividade)),
                    City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null,
                    NameCityCountry = cidadePais
                }.Save();

                return ContentJsonResult(new ExtraActivity(atividadeExtra));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new ExtraActivity() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarAtividadeExtra(string chaveUsuario, int idAtividadeExtra, string cidadePais, string nomeCidade, string tipoAtividade, DateTime dataInicial, DateTime? dataFinal, string pais, string descricao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Usuário não encontrado" });

            var atividadeExtra = TExtraActivity.Load(idAtividadeExtra);
            if (atividadeExtra == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Atividade extra não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new ExtraActivity() { Error = "Local não encontrado" });

            try
            {
                atividadeExtra.Description = descricao;
                atividadeExtra.EndDate = dataFinal;
                atividadeExtra.InitialDate = dataInicial;
                atividadeExtra.EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));
                atividadeExtra.EnumTypeActivity = Enum.GetValues(typeof(TypeActivity)).Cast<TypeActivity>().FirstOrDefault(x => x.Description().Equals(tipoAtividade));
                atividadeExtra.City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null;
                atividadeExtra.NameCityCountry = cidadePais;
                atividadeExtra.Update();

                return ContentJsonResult(new ExtraActivity(atividadeExtra));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new ExtraActivity() { Error = "Por favor tente novamente" });
            }


        }

        public virtual ActionResult ExcluirAtividadeExtra(string chaveUsuario, int idAtividadeExtra)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Usuário não encontrado" });

            var atividadeExtra = TExtraActivity.Load(idAtividadeExtra);
            if (atividadeExtra == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Atividade extra não encontrado" });

            try
            {
                atividadeExtra.Delete();
                return ContentJsonResult(new ExtraActivity() { Success = "Atividade extra excluída com sucesso" });

            }
            catch (Exception e)
            {
                return ContentJsonResult(new ExtraActivity() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarAtividadesExtras(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ExtraActivity() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new ExtraActivities() { Error = "Currículo não encontrado" });

            var atividadeExtras = TExtraActivity.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new ExtraActivities(atividadeExtras));
        }
        #endregion

        #region Cursos Profissionalizantes
        public virtual ActionResult DetalhesCursosProfissionalizantes(string chaveUsuario, int idCurso)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Usuário não encontrado" });

            var curso = TProfessionalCourse.Load(idCurso);
            if (curso == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Curso não encontrado" });

            return ContentJsonResult(new ProfessionalCourse(curso));
        }

        public virtual ActionResult NovoCursoProfissionalizante(string chaveUsuario, int idCurriculo, string nomeCidade, string cidadePais, string instituicao, DateTime? dataConclusao, string pais, string curso, int? cargaHoraria)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Currículo não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new ProfessionalCourse() { Error = "Local não encontrado" });

            try
            {
                var cursoProfissionalizante = new TProfessionalCourse()
                {
                    Course = curso,
                    EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais)),
                    ConclusionDate = dataConclusao == DateTime.MinValue ? null : dataConclusao,
                    Curriculum = curriculo,
                    Institute = instituicao,
                    Workload = cargaHoraria,
                    City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null,
                    NameCityCountry = cidadePais

                }.Save();

                return ContentJsonResult(new ProfessionalCourse(cursoProfissionalizante));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new ProfessionalCourse() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarCursoProfissionalizante(string chaveUsuario, int idCurso, string cidadePais, string nomeCidade, string instituicao, DateTime? dataConclusao, string pais, string curso, int? cargaHoraria)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Usuário não encontrado" });

            var cursoProfissionalizante = TProfessionalCourse.Load(idCurso);
            if (cursoProfissionalizante == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Atividade extra não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new ProfessionalCourse() { Error = "Local não encontrado" });

            try
            {
                cursoProfissionalizante.Institute = instituicao;
                cursoProfissionalizante.ConclusionDate = dataConclusao == DateTime.MinValue ? null : dataConclusao;
                cursoProfissionalizante.Course = curso;
                cursoProfissionalizante.EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));
                cursoProfissionalizante.Workload = cargaHoraria;
                cursoProfissionalizante.NameCityCountry = cidadePais;
                cursoProfissionalizante.City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null;
                cursoProfissionalizante.Update();

                return ContentJsonResult(new ProfessionalCourse(cursoProfissionalizante));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new ProfessionalCourse() { Error = "Por favor tente novamente" });
            }


        }

        public virtual ActionResult ExcluirCursoProfissionalizante(string chaveUsuario, int idCurso)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Usuário não encontrado" });

            var curso = TProfessionalCourse.Load(idCurso);
            if (curso == null)
                return ContentJsonResult(new ProfessionalCourse() { Error = "Curso não encontrado" });

            try
            {
                curso.Delete();
                return ContentJsonResult(new ProfessionalCourse() { Success = "Curso excluído com sucesso" });

            }
            catch (Exception e)
            {
                return ContentJsonResult(new ProfessionalCourse() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarCursosProfissionalizantes(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalCourses() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new ProfessionalCourses() { Error = "Currículo não encontrado" });

            var cursosProfissionalizantes = TProfessionalCourse.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new ProfessionalCourses(cursosProfissionalizantes));
        }
        #endregion

        #region Experiências Profissionais
        public virtual ActionResult DetalhesExperienciaProfissional(string chaveUsuario, int idExperienciaProfissional)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Usuário não encontrado" });

            var experiencia = TProfessionalExperience.Load(idExperienciaProfissional);
            if (experiencia == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Curso não encontrado" });

            return ContentJsonResult(new ProfessionalExperience(experiencia));
        }

        public virtual ActionResult NovoExperienciaProfissional(string chaveUsuario, int idCurriculo, string cidadePais, string nomeCidade, string pais, string empresa, string contatoEmpresa, string atividades, string areaOcupacao, double? ultimoSalario, string motivoDesligamento, DateTime dataAdmissao, DateTime dataDesligamento, string ocupacao, string superiorImediato)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Currículo não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new ProfessionalExperience() { Error = "Local não encontrado" });

            try
            {
                var experiencia = new TProfessionalExperience()
                {
                    ActivitiesPerformed = atividades,
                    EnumOccupationArea = Enum.GetValues(typeof(OccupationArea)).Cast<OccupationArea>().FirstOrDefault(x => x.Description().Equals(areaOcupacao)),
                    AdmissionDate = dataAdmissao,
                    TerminationDate = dataDesligamento,
                    Curriculum = curriculo,
                    Company = empresa,
                    CompanyPhone = contatoEmpresa,
                    ImmediateSuperior = superiorImediato,
                    LastSalary = ultimoSalario,
                    Occupation = ocupacao,
                    NameCityCountry = cidadePais,
                    TerminationReason = motivoDesligamento,
                    EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais)),
                    City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null

                }.Save();

                return ContentJsonResult(new ProfessionalExperience(experiencia));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new ProfessionalExperience() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarExperienciaProfissional(string chaveUsuario, int idExperienciaProfissional, string cidadePais, string nomeCidade, string pais, string empresa, string contatoEmpresa, string atividades, string areaOcupacao, double? ultimoSalario, string motivoDesligamento, DateTime dataAdmissao, DateTime dataDesligamento, string ocupacao, string superiorImediato)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Usuário não encontrado" });

            var experiencia = TProfessionalExperience.Load(idExperienciaProfissional);
            if (experiencia == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Experiência não encontrada" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new ProfessionalExperience() { Error = "Local não encontrado" });

            try
            {
                experiencia.ActivitiesPerformed = atividades;
                experiencia.TerminationDate = dataDesligamento;
                experiencia.AdmissionDate = dataAdmissao;
                experiencia.EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));
                experiencia.City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null;
                experiencia.EnumOccupationArea = Enum.GetValues(typeof(OccupationArea)).Cast<OccupationArea>().FirstOrDefault(x => x.Description().Equals(areaOcupacao));
                experiencia.Company = empresa;
                experiencia.CompanyPhone = contatoEmpresa;
                experiencia.ImmediateSuperior = superiorImediato;
                experiencia.Occupation = ocupacao;
                experiencia.LastSalary = ultimoSalario;
                experiencia.TerminationReason = motivoDesligamento;
                experiencia.NameCityCountry = cidadePais;
                experiencia.Update();

                return ContentJsonResult(new ProfessionalExperience(experiencia));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new ProfessionalExperience() { Error = "Por favor tente novamente" });
            }


        }

        public virtual ActionResult ExcluirExperienciaProfissional(string chaveUsuario, int idExperienciaProfissional)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Usuário não encontrado" });

            var experiencia = TProfessionalExperience.Load(idExperienciaProfissional);
            if (experiencia == null)
                return ContentJsonResult(new ProfessionalExperience() { Error = "Experiência não encontrada" });

            try
            {
                experiencia.Delete();
                return ContentJsonResult(new ProfessionalExperience() { Success = "Experiência excluída com sucesso" });

            }
            catch (Exception e)
            {
                return ContentJsonResult(new ProfessionalCourse() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarExperienciasProfissionais(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new ProfessionalExperiences() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new ProfessionalExperiences() { Error = "Currículo não encontrado" });

            var experienciasProfissionais = TProfessionalExperience.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new ProfessionalExperiences(experienciasProfissionais));
        }
        #endregion

        #region Formação acadêmica
        public virtual ActionResult DetalhesFormacao(string chaveUsuario, int idFormacao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Usuário não encontrado" });

            var formacao = TAcademicFormation.Load(idFormacao);
            if (formacao == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Formação não encontrada" });

            return ContentJsonResult(new AcademicFormation(formacao));
        }

        public virtual ActionResult NovoFormacao(string chaveUsuario, int idCurriculo, string cidadePais, string nomeCidade, string instituicao, string status, string tipoFormacao, DateTime? dataInicial, DateTime? dataFinal, string pais, string curso)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Currículo não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new AcademicFormation() { Error = "Local não encontrado" });

            try
            {
                var formacao = new TAcademicFormation()
                {
                    Course = curso,
                    EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais)),
                    InitialDate = dataInicial == DateTime.MinValue ? null : dataInicial,
                    EndDate = dataFinal == DateTime.MinValue ? null : dataFinal,
                    Curriculum = curriculo,
                    Institute = instituicao,
                    NameCityCountry = cidadePais,
                    EnumStatusFormation = Enum.GetValues(typeof(StatusFormation)).Cast<StatusFormation>().FirstOrDefault(x => x.Description().Equals(status)),
                    EnumTypeFormation = Enum.GetValues(typeof(TypeFormation)).Cast<TypeFormation>().FirstOrDefault(x => x.Description().Equals(tipoFormacao)),
                    City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null

                }.Save();

                return ContentJsonResult(new AcademicFormation(formacao));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new AcademicFormation() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult EditarFormacao(string chaveUsuario, int idFormacao, string cidadePais, string nomeCidade, string instituicao, string status, string tipoFormacao, DateTime? dataInicial, DateTime? dataFinal, string pais, string curso)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Usuário não encontrado" });

            var formacao = TAcademicFormation.Load(idFormacao);
            if (formacao == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Atividade extra não encontrado" });

            var cidade = !string.IsNullOrWhiteSpace(nomeCidade) ? TCity.Find(x => x.CityName.Equals(nomeCidade)) : null;
            if (cidade == null && string.IsNullOrWhiteSpace(cidadePais))
                return ContentJsonResult(new AcademicFormation() { Error = "Local não encontrado" });

            try
            {
                formacao.Institute = instituicao;
                formacao.InitialDate = dataInicial == DateTime.MinValue ? null : dataInicial;
                formacao.EndDate = dataFinal == DateTime.MinValue ? null : dataFinal;
                formacao.Course = curso;
                formacao.EnumCountry = Enum.GetValues(typeof(Country)).Cast<Country>().FirstOrDefault(x => x.Description().Equals(pais));
                formacao.City = !string.IsNullOrWhiteSpace(nomeCidade) ? cidade : null;
                formacao.EnumStatusFormation = Enum.GetValues(typeof(StatusFormation)).Cast<StatusFormation>().FirstOrDefault(x => x.Description().Equals(status));
                formacao.EnumTypeFormation = Enum.GetValues(typeof(TypeFormation)).Cast<TypeFormation>().FirstOrDefault(x => x.Description().Equals(tipoFormacao));
                formacao.NameCityCountry = cidadePais;
                formacao.Update();

                return ContentJsonResult(new AcademicFormation(formacao));
            }
            catch (Exception e)
            {
                return ContentJsonResult(new AcademicFormation() { Error = "Por favor tente novamente" });
            }


        }

        public virtual ActionResult ExcluirFormacao(string chaveUsuario, int idFormacao)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Usuário não encontrado" });

            var formacao = TAcademicFormation.Load(idFormacao);
            if (formacao == null)
                return ContentJsonResult(new AcademicFormation() { Error = "Formação não encontrada" });

            try
            {
                formacao.Delete();
                return ContentJsonResult(new AcademicFormation() { Success = "Formação excluída com sucesso" });

            }
            catch (Exception e)
            {
                return ContentJsonResult(new AcademicFormation() { Error = "Por favor tente novamente" });
            }
        }

        public virtual ActionResult ListarFormacoesAcademicas(string chaveUsuario, int idCurriculo)
        {
            var usuario = TUser.Find(x => x.Token == chaveUsuario);
            if (usuario == null)
                return ContentJsonResult(new AcademicFormations() { Error = "Usuário não encontrado" });

            var curriculo = TCurriculum.Load(idCurriculo);
            if (curriculo == null)
                return ContentJsonResult(new AcademicFormations() { Error = "Currículo não encontrado" });

            var formacoes = TAcademicFormation.List(x => x.Curriculum.Id == curriculo.Id).ToList();

            return ContentJsonResult(new AcademicFormations(formacoes));
        }
        #endregion

        #region Notificação Email

        public virtual ActionResult EnviarEmailTeste()
        {
            try
            {
                new MailService().Send("bruno.skiller@gmail.com", "aaa", "malubrito18@gmail.com", "Brunao", "Marcela pao dura", "<p>MARCELA PÃO DURA</p><br/><br/><p>Enviado do sistema NimbleHunter</p>");
                return ContentJsonResult("true");
            }
            catch (Exception e)
            {
                return ContentJsonResult(e.Message);
            }

        }
        #endregion

        #region Lista de Enum's

        public virtual ActionResult ListarPaises()
        {
            var paises = EnumHelper.ListAll<Country>().Select(x => x.Description()).ToList();
            return ContentJsonResult(paises);
        }

        public virtual ActionResult ListarStatusVaga()
        {
            var status = EnumHelper.ListAll<Status>().Select(x => x.Description()).ToList();
            return ContentJsonResult(status);
        }

        public virtual ActionResult ListarNivelIdioma()
        {
            var niveis = EnumHelper.ListAll<LevelLanguage>().Select(x => x.Description()).ToList();
            return ContentJsonResult(niveis);
        }

        public virtual ActionResult ListarGrausParentesco()
        {
            var graus = EnumHelper.ListAll<DegreeKinship>().Select(x => x.Description()).ToList();
            return ContentJsonResult(graus);
        }

        public virtual ActionResult ListarAreasInteresse()
        {
            var areas = EnumHelper.ListAll<AreaInterest>().Select(x => x.Description()).ToList();
            return ContentJsonResult(areas);
        }

        public virtual ActionResult ListarGeneros()
        {
            var generos = EnumHelper.ListAll<Genre>().Select(x => x.Description()).ToList();
            return ContentJsonResult(generos);
        }

        public virtual ActionResult ListarIdiomas()
        {
            var idiomas = EnumHelper.ListAll<Language>().Select(x => x.Description()).ToList();
            return ContentJsonResult(idiomas);
        }

        public virtual ActionResult ListarNiveisInformatica()
        {
            var niveis = EnumHelper.ListAll<Level>().Select(x => x.Description()).ToList();
            return ContentJsonResult(niveis);
        }

        public virtual ActionResult ListarAreasNegocio()
        {
            var linhas = EnumHelper.ListAll<LineBusiness>().Select(x => x.Description()).ToList();
            return ContentJsonResult(linhas);
        }

        public virtual ActionResult ListarEstadoscivis()
        {
            var estados = EnumHelper.ListAll<MaritalStatus>().Select(x => x.Description()).ToList();
            return ContentJsonResult(estados);
        }

        public virtual ActionResult ListarAreasOcupacao()
        {
            var areas = EnumHelper.ListAll<OccupationArea>().Select(x => x.Description()).ToList();
            return ContentJsonResult(areas);
        }

        public virtual ActionResult ListarEtapasVagas()
        {
            var etapas = EnumHelper.ListAll<Step>().Select(x => x.Description()).ToList();
            return ContentJsonResult(etapas);
        }

        public virtual ActionResult ListarTiposCampoAdicional()
        {
            var tipos = EnumHelper.ListAll<TypeField>().Select(x => x.Description()).ToList();
            return ContentJsonResult(tipos);
        }

        public virtual ActionResult ListarTiposAtividade()
        {
            var tipos = EnumHelper.ListAll<TypeActivity>().Select(x => x.Description()).ToList();
            return ContentJsonResult(tipos);
        }

        public virtual ActionResult ListarTiposFormacao()
        {
            var tipos = EnumHelper.ListAll<TypeFormation>().Select(x => x.Description()).ToList();
            return ContentJsonResult(tipos);
        }

        public virtual ActionResult ListarFaixaSalarioPretendido()
        {
            var salarios = EnumHelper.ListAll<SalaryRequeriments>().Select(x => x.Description()).ToList();
            return ContentJsonResult(salarios);
        }

        public virtual ActionResult ListarStatusFormacao()
        {
            var status = EnumHelper.ListAll<StatusFormation>().Select(x => x.Description()).ToList();
            return ContentJsonResult(status);
        }
        #endregion

        #region Componentes

        public virtual ActionResult ListarEstados()
        {
            var estados = TState.ListAll().Select(x => x.Sigla);
            return ContentJsonResult(estados);
        }

        [AllowCors]
        public virtual JsonResult ListarCidades(string estado)
        {
            var cidades = TCity.List(x => x.State.Sigla.Equals(estado));
            List<Object> jsonCidades = new List<object>();
            foreach (var cidade in cidades)
            {
                jsonCidades.Add(new
                {
                    name = cidade.CityName
                });
            }
            return Json(jsonCidades, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult ListarCidadesAction(string estado)
        {
            var cidades = TCity.List(x => x.State.Sigla.Equals(estado)).Select(x => x.CityName).ToList();
            return ContentJsonResult(cidades);
        }
        #endregion

        private ContentResult ContentJsonResult(object obj)
        {
            return Content(obj.ToJSON(false), "application/json");
        }
    }
}