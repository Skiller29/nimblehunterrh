﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NimbleHunterRH.Domain;

namespace NimbleHunterRH.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class RequiresAuthorizationAttribute : ActionFilterAttribute, IAuthorizationFilter, IExceptionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SecurityContext.Do.Init(() => filterContext.HttpContext.User.Identity);

            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var security = SecurityContext.Do;

            bool isLoggedIn = security.IsAuthenticated;

            if (!isLoggedIn)
                NoLogged(filterContext);

            base.OnActionExecuting(filterContext);
        }

        private static void NoLogged(ControllerContext filterContext)
        {
            var rUrl = "";
            if (!string.IsNullOrWhiteSpace(filterContext.HttpContext.Request.QueryString["returnUrl"]))
                rUrl = filterContext.HttpContext.Request.QueryString["returnUrl"];
            else
                rUrl = filterContext.HttpContext.Request.Url.AbsoluteUri;

            filterContext.HttpContext.Response.Redirect(new UrlHelper(filterContext.RequestContext)
                .Action("Login", "API", new { returnUrl = rUrl }));
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
        }

        #region IExceptionFilter Members

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is AuthorizationException)
                NoLogged(filterContext);
        }

        #endregion
    }
}
