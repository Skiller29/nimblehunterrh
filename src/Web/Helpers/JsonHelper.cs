﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Globalization;

namespace NimbleHunterRH.Web.Helpers
{
    public static class JsonHelper
    {
        public static string ToJSON(this object obj, bool debug = false, bool lowerCase = false)
        {
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore};
            if (lowerCase) settings.ContractResolver = new LowercaseContractResolver();
            return JsonConvert.SerializeObject(obj, debug ? Formatting.Indented : Formatting.None, settings).Replace(@"\""", @"""");
        }

        public static readonly JsonSerializerSettings DefaultSerializerSettings;

        static JsonHelper()
        {
            DefaultSerializerSettings = new JsonSerializerSettings { ContractResolver = new LowercaseContractResolver() };


            var timeConverter = new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeLocal };

            DefaultSerializerSettings.Converters.Add(timeConverter);
            //DefaultSerializerSettings.Converters.Add(new ByteArrayConverter());

            DefaultSerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            DefaultSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            DefaultSerializerSettings.NullValueHandling = NullValueHandling.Include;
            DefaultSerializerSettings.DefaultValueHandling = DefaultValueHandling.Include;
            DefaultSerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
        }

        public static object JsonAlert(string type, string message)
        {
            return new { type, message };
        }
    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}
